//
//  MessageService.swift
//  Simple
//
//  Created by Nikita Bondar on 12.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let MessageDidGetted             = Notification.Name("MessageDidGetted")
    static let MessageDidNotGetted          = Notification.Name("MessageDidNotGetted")
    
    static let MessagesDidGetted            = Notification.Name("MessagesDidGetted")
    static let MessagesDidNotGetted         = Notification.Name("MessagesDidNotGetted")
    
    static let MessagesLastDidGetted        = Notification.Name("MessagesLastDidGetted")
    static let MessagesLastDidNotGetted     = Notification.Name("MessagesLastDidNotGetted")
    
    static let MessagesDidGettedUpdates     = Notification.Name("MessagesDidGettedUpdates")
    static let MessagesDidNotGettedUpdates  = Notification.Name("MessagesDidNotGettedUpdates")
    
    static let MessageDidAdded              = Notification.Name("MessageDidAdded")
    static let MessageDidNotAdded           = Notification.Name("MessageDidNotAdded")
    
    static let MessageDidEdited             = Notification.Name("MessageDidEdited")
    static let MessageDidNotEdited          = Notification.Name("MessageDidNotEdited")
    
    static let MessageDidDeleted            = Notification.Name("MessageDidDeleted")
    static let MessageDidNotDeleted         = Notification.Name("MessageDidNotDeleted")
}

class MessageService {
    
    // MARK: Static part
    
    private static var service: MessageService?
    public static var shared: MessageService {
        if let service = service {
            return service
        }
        let accToken = UserManager.shared.current?.accToken ?? ""
        service = MessageService(accToken: accToken)
        return service!
    }
    
    // MARK: gRPC part
    
    private let address: String = {
        var address = "195.24.64.97"
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path), let mode = dict["Application mode"] as? String {
            address += (mode == "development" ? ":228" : ":1488")
        }
        return address
    }()
    
    private lazy var service: message_MesServiceClient = message_MesServiceClient(address: address, secure: false, arguments: [])
    
    // MARK: Factories
    
    private let reqFactory = RequestFactory()
    
    // MARK: Queue
    
    private let queue = DispatchQueue(label: "com.message.queue", qos: .userInitiated, attributes: .concurrent)
    
    // MARK: System
    
    private var accToken: String
    
    public init(accToken: String) {
        self.accToken = accToken
    }
    
    public func update(accToken: String) {
        self.accToken = accToken
    }
    
    // MARK: Main methods
    
    public func get(id: String) {
        queue.async {
            let req = self.reqFactory.mesGetMessageReq(accToken: self.accToken, id: id)
            
            do {
                _ = try self.service.getMessage(req, completion: { (resp, res) in
                    if !self.isForbidden(with: resp?.code) {
                        print(res.statusCode)
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .MessageDidGetted, object: self, userInfo: ["message": resp!.message])
                        } else {
                            NotificationCenter.default.post(name: .MessageDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func get(cid: String, page: Int) {
        queue.async {
            let req = self.reqFactory.mesGetMessagesReq(accToken: self.accToken, cid: cid, page: page)
            
            do {
                _ = try self.service.getMessages(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .MessagesDidGetted, object: self, userInfo: ["messages": resp!.messages])
                        } else {
                            NotificationCenter.default.post(name: .MessagesDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func getLast(cids: [String]) {
        queue.async {
            let req = self.reqFactory.mesGetLastMessagesReq(accToken: self.accToken, cids: cids)
            
            do {
                _ = try self.service.getLastMessages(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .MessagesLastDidGetted, object: self, userInfo: ["messages": resp!.messages])
                        } else {
                            NotificationCenter.default.post(name: .MessagesLastDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func getUpdates(chatId: String, date: Int) {
        queue.async {
            let req = self.reqFactory.mesGetUpdatesReq(accToken: self.accToken, chatId: chatId, date: date)
            
            do {
                _ = try self.service.getUpdates(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .MessagesDidGettedUpdates, object: self, userInfo: ["messages": resp!.messages])
                        } else {
                            NotificationCenter.default.post(name: .MessagesDidNotGettedUpdates, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func add(message: Message) {
        queue.async {
            let req = self.reqFactory.mesAddMessageReq(accToken: self.accToken, cid: message.chatId, text: message.text, mids: message.messageIds(), token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.addMessage(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .MessageDidAdded, object: self, userInfo: ["id": resp!.id, "date": resp!.date, "message": message])
                        } else {
                            NotificationCenter.default.post(name: .MessageDidNotAdded, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func edit(message: Message) {
        queue.async {
            let req = self.reqFactory.mesEditMessageReq(accToken: self.accToken, message: message, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.editMessage(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .MessageDidEdited, object: self)
                        } else {
                            NotificationCenter.default.post(name: .MessageDidNotEdited, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func del(id: String) {
        queue.async {
            let req = self.reqFactory.mesDelMessageReq(accToken: self.accToken, id: id, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.delMessage(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .MessageDidDeleted, object: self)
                        } else {
                            NotificationCenter.default.post(name: .MessageDidNotDeleted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: Helps
    
    private func sendForbidden() {
        NotificationCenter.default.post(name: .PermissionDenied, object: self)
    }
    
    private func isForbidden(with code: message_StatusCode?) -> Bool {
        return code != nil && code!.value == StatusCodes.PermissionDenied.rawValue
    }
    
}
