//
// DO NOT EDIT.
//
// Generated by the protocol buffer compiler.
// Source: Simple/Services/Message/Proto/message.proto
//

//
// Copyright 2018, gRPC Authors All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
import Foundation
import Dispatch
import SwiftGRPC
import SwiftProtobuf

internal protocol message_MesGetMessageCall: ClientCallUnary {}

fileprivate final class message_MesGetMessageCallBase: ClientCallUnaryBase<message_GetMessageReq, message_GetMessageResp>, message_MesGetMessageCall {
  override class var method: String { return "/message.proto.Mes/GetMessage" }
}

internal protocol message_MesGetMessagesCall: ClientCallUnary {}

fileprivate final class message_MesGetMessagesCallBase: ClientCallUnaryBase<message_GetMessagesReq, message_GetMessagesResp>, message_MesGetMessagesCall {
  override class var method: String { return "/message.proto.Mes/GetMessages" }
}

internal protocol message_MesGetLastMessagesCall: ClientCallUnary {}

fileprivate final class message_MesGetLastMessagesCallBase: ClientCallUnaryBase<message_GetLastMessagesReq, message_GetLastMessagesResp>, message_MesGetLastMessagesCall {
  override class var method: String { return "/message.proto.Mes/GetLastMessages" }
}

internal protocol message_MesGetUpdatesCall: ClientCallUnary {}

fileprivate final class message_MesGetUpdatesCallBase: ClientCallUnaryBase<message_GetUpdatesReq, message_GetUpdatesResp>, message_MesGetUpdatesCall {
  override class var method: String { return "/message.proto.Mes/GetUpdates" }
}

internal protocol message_MesAddMessageCall: ClientCallUnary {}

fileprivate final class message_MesAddMessageCallBase: ClientCallUnaryBase<message_AddMessageReq, message_AddMessageResp>, message_MesAddMessageCall {
  override class var method: String { return "/message.proto.Mes/AddMessage" }
}

internal protocol message_MesEditMessageCall: ClientCallUnary {}

fileprivate final class message_MesEditMessageCallBase: ClientCallUnaryBase<message_EditMessageReq, message_EditMessageResp>, message_MesEditMessageCall {
  override class var method: String { return "/message.proto.Mes/EditMessage" }
}

internal protocol message_MesDelMessageCall: ClientCallUnary {}

fileprivate final class message_MesDelMessageCallBase: ClientCallUnaryBase<message_DelMessageReq, message_DelMessageResp>, message_MesDelMessageCall {
  override class var method: String { return "/message.proto.Mes/DelMessage" }
}


/// Instantiate message_MesServiceClient, then call methods of this protocol to make API calls.
internal protocol message_MesService: ServiceClient {
  /// Synchronous. Unary.
  func getMessage(_ request: message_GetMessageReq) throws -> message_GetMessageResp
  /// Asynchronous. Unary.
  func getMessage(_ request: message_GetMessageReq, completion: @escaping (message_GetMessageResp?, CallResult) -> Void) throws -> message_MesGetMessageCall

  /// Synchronous. Unary.
  func getMessages(_ request: message_GetMessagesReq) throws -> message_GetMessagesResp
  /// Asynchronous. Unary.
  func getMessages(_ request: message_GetMessagesReq, completion: @escaping (message_GetMessagesResp?, CallResult) -> Void) throws -> message_MesGetMessagesCall

  /// Synchronous. Unary.
  func getLastMessages(_ request: message_GetLastMessagesReq) throws -> message_GetLastMessagesResp
  /// Asynchronous. Unary.
  func getLastMessages(_ request: message_GetLastMessagesReq, completion: @escaping (message_GetLastMessagesResp?, CallResult) -> Void) throws -> message_MesGetLastMessagesCall

  /// Synchronous. Unary.
  func getUpdates(_ request: message_GetUpdatesReq) throws -> message_GetUpdatesResp
  /// Asynchronous. Unary.
  func getUpdates(_ request: message_GetUpdatesReq, completion: @escaping (message_GetUpdatesResp?, CallResult) -> Void) throws -> message_MesGetUpdatesCall

  /// Synchronous. Unary.
  func addMessage(_ request: message_AddMessageReq) throws -> message_AddMessageResp
  /// Asynchronous. Unary.
  func addMessage(_ request: message_AddMessageReq, completion: @escaping (message_AddMessageResp?, CallResult) -> Void) throws -> message_MesAddMessageCall

  /// Synchronous. Unary.
  func editMessage(_ request: message_EditMessageReq) throws -> message_EditMessageResp
  /// Asynchronous. Unary.
  func editMessage(_ request: message_EditMessageReq, completion: @escaping (message_EditMessageResp?, CallResult) -> Void) throws -> message_MesEditMessageCall

  /// Synchronous. Unary.
  func delMessage(_ request: message_DelMessageReq) throws -> message_DelMessageResp
  /// Asynchronous. Unary.
  func delMessage(_ request: message_DelMessageReq, completion: @escaping (message_DelMessageResp?, CallResult) -> Void) throws -> message_MesDelMessageCall

}

internal final class message_MesServiceClient: ServiceClientBase, message_MesService {
  /// Synchronous. Unary.
  internal func getMessage(_ request: message_GetMessageReq) throws -> message_GetMessageResp {
    return try message_MesGetMessageCallBase(channel)
      .run(request: request, metadata: metadata)
  }
  /// Asynchronous. Unary.
  internal func getMessage(_ request: message_GetMessageReq, completion: @escaping (message_GetMessageResp?, CallResult) -> Void) throws -> message_MesGetMessageCall {
    return try message_MesGetMessageCallBase(channel)
      .start(request: request, metadata: metadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func getMessages(_ request: message_GetMessagesReq) throws -> message_GetMessagesResp {
    return try message_MesGetMessagesCallBase(channel)
      .run(request: request, metadata: metadata)
  }
  /// Asynchronous. Unary.
  internal func getMessages(_ request: message_GetMessagesReq, completion: @escaping (message_GetMessagesResp?, CallResult) -> Void) throws -> message_MesGetMessagesCall {
    return try message_MesGetMessagesCallBase(channel)
      .start(request: request, metadata: metadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func getLastMessages(_ request: message_GetLastMessagesReq) throws -> message_GetLastMessagesResp {
    return try message_MesGetLastMessagesCallBase(channel)
      .run(request: request, metadata: metadata)
  }
  /// Asynchronous. Unary.
  internal func getLastMessages(_ request: message_GetLastMessagesReq, completion: @escaping (message_GetLastMessagesResp?, CallResult) -> Void) throws -> message_MesGetLastMessagesCall {
    return try message_MesGetLastMessagesCallBase(channel)
      .start(request: request, metadata: metadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func getUpdates(_ request: message_GetUpdatesReq) throws -> message_GetUpdatesResp {
    return try message_MesGetUpdatesCallBase(channel)
      .run(request: request, metadata: metadata)
  }
  /// Asynchronous. Unary.
  internal func getUpdates(_ request: message_GetUpdatesReq, completion: @escaping (message_GetUpdatesResp?, CallResult) -> Void) throws -> message_MesGetUpdatesCall {
    return try message_MesGetUpdatesCallBase(channel)
      .start(request: request, metadata: metadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func addMessage(_ request: message_AddMessageReq) throws -> message_AddMessageResp {
    return try message_MesAddMessageCallBase(channel)
      .run(request: request, metadata: metadata)
  }
  /// Asynchronous. Unary.
  internal func addMessage(_ request: message_AddMessageReq, completion: @escaping (message_AddMessageResp?, CallResult) -> Void) throws -> message_MesAddMessageCall {
    return try message_MesAddMessageCallBase(channel)
      .start(request: request, metadata: metadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func editMessage(_ request: message_EditMessageReq) throws -> message_EditMessageResp {
    return try message_MesEditMessageCallBase(channel)
      .run(request: request, metadata: metadata)
  }
  /// Asynchronous. Unary.
  internal func editMessage(_ request: message_EditMessageReq, completion: @escaping (message_EditMessageResp?, CallResult) -> Void) throws -> message_MesEditMessageCall {
    return try message_MesEditMessageCallBase(channel)
      .start(request: request, metadata: metadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func delMessage(_ request: message_DelMessageReq) throws -> message_DelMessageResp {
    return try message_MesDelMessageCallBase(channel)
      .run(request: request, metadata: metadata)
  }
  /// Asynchronous. Unary.
  internal func delMessage(_ request: message_DelMessageReq, completion: @escaping (message_DelMessageResp?, CallResult) -> Void) throws -> message_MesDelMessageCall {
    return try message_MesDelMessageCallBase(channel)
      .start(request: request, metadata: metadata, completion: completion)
  }

}

