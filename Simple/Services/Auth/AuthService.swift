//
//  AuthService.swift
//  Simple
//
//  Created by Nikita Bondar on 07.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let DidLogined               = Notification.Name("DidLogined")
    static let DidNotLogined            = Notification.Name("DidNotLogined")
    
    static let DidVerified              = Notification.Name("DidVerified")
    static let DidNotVerified           = Notification.Name("DidNotVerified")
    
    static let DidRegistered            = Notification.Name("DidRegistered")
    static let DidNotRegistered         = Notification.Name("DidNotRegistered")

    static let PermissionDenied         = Notification.Name("PermissionDenied")
    static let InvalidToken             = Notification.Name("InvalidToken")
    
    static let DidLogouted              = Notification.Name("DidLogouted")
    static let DidNotLogouted           = Notification.Name("DidNotLogouted")
    
    static let DidRefreshed             = Notification.Name("DidRefreshed")
    static let DidNotRefreshed          = Notification.Name("DidNotRefreshed")
}

class AuthService {
    
    // MARK: Static part
    
    private static var service: AuthService?
    public static var shared: AuthService {
        if let service = service {
            return service
        }
        service = AuthService()
        return service!
    }
    
    // MARK: gRPC part
    
    private let address: String = {
        var address = "195.24.64.97"
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path), let mode = dict["Application mode"] as? String {
            address += (mode == "development" ? ":228" : ":1488")
        }
        return address
    }()
    
    private lazy var service: auth_AuthServiceClient = auth_AuthServiceClient(address: address, secure: false, arguments: [])
    
    // MARK: Factories
    
    private let userFactory = UserFactory()
    private let reqFactory = RequestFactory()
    
    // MARK: Queue
    
    private let queue = DispatchQueue(label: "com.auth.queue", qos: .userInitiated, attributes: .concurrent)
    
    // MARK: Main methods
    
    public func login(with phone: Phone) {
        queue.async {
            let req = self.reqFactory.authLoginReq(with: phone)
            
            do {
                _ = try self.service.login(req, completion: { (resp, _) in
                    if let val = resp?.value, val != 0 {
                        NotificationCenter.default.post(name: .DidLogined, object: self)
                    } else {
                        NotificationCenter.default.post(name: .DidNotLogined, object: self)
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func verify(phone: Phone, with code: Code) {
        queue.async {
            let req = self.reqFactory.authVerifyReq(with: phone, and: code)
            
            do {
                _ = try self.service.verify(req, completion: { (resp, _) in
                    if let val = resp?.result.value, val != 0 {
                        NotificationCenter.default.post(name: .DidVerified, object: self, userInfo: ["user": resp!.user])
                    } else {
                        NotificationCenter.default.post(name: .DidNotVerified, object: self)
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func login(username: String, password: String) {
        queue.async {
            let req = self.reqFactory.authLoginWithoutPhoneReq(username: username, password: password)
            
            do {
                _ = try self.service.loginWithoutPhone(req, completion: { (resp, _) in
                    
                    if let val = resp?.result.value, val != 0 {
                        NotificationCenter.default.post(name: .DidLogined, object: self, userInfo: ["user": resp!.user])
                    } else {
                        NotificationCenter.default.post(name: .DidNotLogined, object: self)
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func register(username: String, password: String) {
        queue.async {
            let req = self.reqFactory.authRegisterReq(username: username, password: password)
            
            do {
                _ = try self.service.register(req, completion: { (resp, _) in
                    if let val = resp?.result.value, val != 0 {
                        NotificationCenter.default.post(name: .DidRegistered, object: self)
                    } else {
                        NotificationCenter.default.post(name: .DidNotRegistered, object: self)
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func logout() {
        queue.async {
            if let user = UserManager.shared.current {
                let req = self.reqFactory.authLogoutReq(user: user, devToken: PushCenter.shared.getToken())
                
                do {
                    _ = try self.service.logout(req, completion: { (resp, _) in
                        if let code = resp?.code, code.value != StatusCodes.PermissionDenied.rawValue {
                            if let val = resp?.result.value, val != 0 {
                                NotificationCenter.default.post(name: .DidLogouted, object: self)
                            } else {
                                NotificationCenter.default.post(name: .DidNotLogouted, object: self)
                            }
                        } else {
                            NotificationCenter.default.post(name: .PermissionDenied, object: self)
                        }
                    })
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    public func refresh() {
        queue.async {
            if let user = UserManager.shared.current {
                let req = self.reqFactory.authRefReq(user: user)
                
                do {
                    _ = try self.service.refresh(req, completion: { (resp, _) in
                        if let code = resp?.code, code.value != StatusCodes.PermissionDenied.rawValue {
                            if let val = resp?.result.value, val != 0 {
                                NotificationCenter.default.post(name: .DidRefreshed, object: self, userInfo: ["accToken": resp!.accToken, "refToken": resp!.refToken])
                            } else {
                                NotificationCenter.default.post(name: .DidNotRefreshed, object: self)
                            }
                        } else {
                            NotificationCenter.default.post(name: .InvalidToken, object: self)
                        }
                    })
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
}
