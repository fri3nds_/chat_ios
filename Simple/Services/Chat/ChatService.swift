//
//  ChatService.swift
//  Simple
//
//  Created by Nikita Bondar on 12.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let ChatDidGetted            = Notification.Name("ChatDidGetted")
    static let ChatDidNotGetted         = Notification.Name("ChatDidNotGetted")
    
    static let ChatsDidGetted           = Notification.Name("ChatsDidGetted")
    static let ChatsDidNotGetted        = Notification.Name("ChatsDidNotGetted")
    
    static let ChatDidAdded             = Notification.Name("ChatDidAdded")
    static let ChatDidNotAdded          = Notification.Name("ChatDidNotAdded")
    
    static let ChatDidEdited            = Notification.Name("ChatDidEdited")
    static let ChatDidNotEdited         = Notification.Name("ChatDidNotEdited")
    
    static let ChatUserDidAdded         = Notification.Name("ChatUserDidAdded")
    static let ChatUserDidNotAdded      = Notification.Name("ChatUserDidNotAdded")
    
    static let ChatUserDidDeleted       = Notification.Name("ChatUserDidDeleted")
    static let ChatUserDidNotDeleted    = Notification.Name("ChatUserDidNotDeleted")
    
    static let ChatLastDidUpdate        = Notification.Name("ChatLastDidUpdate")
    static let ChatLastDidNotUpdate     = Notification.Name("ChatLastDidNotUpdate")
}

class ChatService {
    
    // MARK: Static part
    
    private static var service: ChatService?
    public static var shared: ChatService {
        if let service = service {
            return service
        }
        let accToken = UserManager.shared.current?.accToken ?? ""
        service = ChatService(accToken: accToken)
        return service!
    }
    
    // MARK: gRPC part
    
    private let address: String = {
        var address = "195.24.64.97"
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path), let mode = dict["Application mode"] as? String {
            address += (mode == "development" ? ":228" : ":1488")
        }
        return address
    }()
    
    private lazy var service: chat_ChServiceClient = chat_ChServiceClient(address: address, secure: false, arguments: [])
    
    // MARK: Factories
    
    private let reqFactory = RequestFactory()
    
    // MARK: Queue
    
    private let queue = DispatchQueue(label: "com.chat.queue", qos: .userInitiated, attributes: .concurrent)
    
    // MARK: System
    
    private var accToken: String
    
    public init(accToken: String) {
        self.accToken = accToken
    }
    
    public func update(accToken: String) {
        self.accToken = accToken
    }
    
    // MARK: Main methods
    
    public func get(id: String) {
        queue.async {
            let req = self.reqFactory.chatGetChatReq(accToken: self.accToken, id: id)
            
            do {
                _ = try self.service.getChat(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatDidGetted, object: self, userInfo: ["chat": resp!.chat])
                        } else {
                            NotificationCenter.default.post(name: .ChatDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func get(with friend: String) {
        queue.async {
            let req = self.reqFactory.chatGetChatWithReq(accToken: self.accToken, uid: friend)
            
            do {
                _ = try self.service.getChatWith(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatDidGetted, object: self, userInfo: ["chat": resp!.chat])
                        } else {
                            NotificationCenter.default.post(name: .ChatDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func getChats() {
        queue.async {
            let req = self.reqFactory.chatGetChatsReq(accToken: self.accToken)
            
            do {
                _ = try self.service.getChats(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatsDidGetted, object: self, userInfo: ["chats": resp!.chats])
                        } else {
                            NotificationCenter.default.post(name: .ChatsDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func addDialog(friend: String, completion: ((chat_Chat) -> ())? = nil) {
        queue.async {
            let req = self.reqFactory.chatAddDialogReq(accToken: self.accToken, uid: friend, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.addDialog(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            completion?(resp!.chat)
                            NotificationCenter.default.post(name: .ChatDidAdded, object: self, userInfo: ["chat": resp!.chat])
                        } else {
                            NotificationCenter.default.post(name: .ChatDidNotAdded, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func addGroup(title: String, friends: [String]) {
        queue.async {
            let req = self.reqFactory.chatAddGroupReq(accToken: self.accToken, title: title, uids: friends, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.addGroup(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatDidAdded, object: self, userInfo: ["chat": resp!.chat])
                        } else {
                            NotificationCenter.default.post(name: .ChatDidNotAdded, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func addChannel(title: String) {
        queue.async {
            let req = self.reqFactory.chatAddChannelReq(accToken: self.accToken, title: title, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.addChannel(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatDidAdded, object: self, userInfo: ["chat": resp!.chat])
                        } else {
                            NotificationCenter.default.post(name: .ChatDidNotAdded, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func edit(chat: String, title: String) {
        queue.async {
            let req = self.reqFactory.chatEditChatReq(accToken: self.accToken, id: chat, title: title, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.editChat(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatDidEdited, object: self)
                        } else {
                            NotificationCenter.default.post(name: .ChatDidNotEdited, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func add(user: String, to chat: String) {
        queue.async {
            let req = self.reqFactory.chatAddUserReq(accToken: self.accToken, id: chat, uid: user, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.addUser(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatUserDidAdded, object: self)
                        } else {
                            NotificationCenter.default.post(name: .ChatUserDidNotAdded, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func del(user: String, from chat: String) {
        queue.async {
            let req = self.reqFactory.chatDelUserReq(accToken: self.accToken, id: chat, uid: user, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.delUser(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatUserDidDeleted, object: self)
                        } else {
                            NotificationCenter.default.post(name: .ChatUserDidNotDeleted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func updateLast(chatId: String) {
        queue.async {
            let req = self.reqFactory.chatUpdateLast(accToken: self.accToken, chatId: chatId, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.updateLast(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .ChatLastDidUpdate, object: self, userInfo: ["chatId": chatId, "date": resp!.date])
                        } else {
                            NotificationCenter.default.post(name: .ChatLastDidNotUpdate, object: self)
                        }
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: Helps
    
    private func sendForbidden() {
        NotificationCenter.default.post(name: .PermissionDenied, object: self)
    }
    
    private func isForbidden(with code: chat_StatusCode?) -> Bool {
        return code != nil && code!.value == StatusCodes.PermissionDenied.rawValue
    }
    
}
