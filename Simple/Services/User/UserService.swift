//
//  UserService.swift
//  Simple
//
//  Created by Nikita Bondar on 11.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let UserDidGetted                = Notification.Name("UserDidGetted")
    static let UserDidNotGetted             = Notification.Name("UserDidNotGetted")
    
    static let UsersDidGetted               = Notification.Name("UsersDidGetted")
    static let UsersDidNotGetted            = Notification.Name("UsersDidNotGetted")
    
    static let UsersDidSearched             = Notification.Name("UsersDidSearched")
    static let UsersDidNotSearched          = Notification.Name("UsersDidNotSearched")
    
    static let UserNameDidEdited            = Notification.Name("UserNameDidEdited")
    static let UserNameDidNotEdited         = Notification.Name("UserNameDidNotEdited")
    
    static let UserUsernameDidEdited        = Notification.Name("UserUsernameDidEdited")
    static let UserUsernameDidNotEdited     = Notification.Name("UserUsernameDidNotEdited")
    
    static let UserPhoneDidEdited           = Notification.Name("UserPhoneDidEdited")
    static let UserPhoneDidNotEdited        = Notification.Name("UserPhoneDidNotEdited")
    
    static let UserPasswordDidEdited        = Notification.Name("UserPasswordDidEdited")
    static let UserPasswordDidNotEdited     = Notification.Name("UserPasswordDidNotEdited")
    
    static let UserStatusDidChanged         = Notification.Name("UserStatusDidChanged")
    static let UserStatusDidNotChanged      = Notification.Name("UserStatusDidNotChanged")
    
    static let UserTokenDidAdded            = Notification.Name("UserTokenDidAdded")
    static let UserTokenDidNotAdded         = Notification.Name("UserTokenDidNotAdded")
    
    static let UserTokenDidDeleted          = Notification.Name("UserTokenDidDeleted")
    static let UserTokenDidNotDeleted       = Notification.Name("UserTokenDidNotDeleted")
}

class UserService {
    
    // MARK: Static part
    
    private static var service: UserService?
    public static var shared: UserService {
        if let service = service {
            return service
        }
        let accToken = UserManager.shared.current?.accToken ?? ""
        service = UserService(accToken: accToken)
        return service!
    }
    
    // MARK: Properties
    
    private let address: String = {
        var address = "195.24.64.97"
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path), let mode = dict["Application mode"] as? String {
            address += (mode == "development" ? ":228" : ":1488")
        }
        return address
    }()
    
    private lazy var service: user_UsServiceClient = user_UsServiceClient(address: address, secure: false, arguments: [])
   
    // MARK: Factories

    private var reqFactory: RequestFactory = RequestFactory()
    
    // MARK: Queue
    
    private let queue = DispatchQueue(label: "com.user.queue", qos: .userInitiated, attributes: .concurrent)
    
    // MARK: System
    
    private var accToken: String
    
    public init(accToken: String) {
        self.accToken = accToken
    }
    
    public func update(accToken: String) {
        self.accToken = accToken
    }
    
    // MARK: Main methods
    
    public func get(id: String, completion: ((_ user: user_User) -> Void)? = nil) {
        queue.async {
            let req = self.reqFactory.userGetReq(accToken: self.accToken, id: id)
            
            do {
                _ = try self.service.get(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            completion?(resp!.user)
                            NotificationCenter.default.post(name: .UserDidGetted, object: self, userInfo: ["user": resp!.user])
                        } else {
                            NotificationCenter.default.post(name: .UserDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func get(username: String) {
        queue.async {
            let req = self.reqFactory.userGetByUsernameReq(accToken: self.accToken, username: username)
            
            do {
                _ = try self.service.getByUsername(req, completion: { (resp, _) in
                    
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserDidGetted, object: self, userInfo: ["user": resp!.user])
                        } else {
                            NotificationCenter.default.post(name: .UserDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func get(phone: Phone) {
        queue.async {
            let req = self.reqFactory.userGetByPhoneReq(accToken: self.accToken, phone: phone)
            
            do {
                _ = try self.service.getByPhone(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserDidGetted, object: self, userInfo: ["user": resp!.user])
                        } else {
                            NotificationCenter.default.post(name: .UserDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func get(phones: [Phone]) {
        queue.async {
            let req = self.reqFactory.userGetByPhonesReq(accToken: self.accToken, phones: phones)
            
            do {
                _ = try self.service.getByPhones(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UsersDidGetted, object: self, userInfo: ["users": resp!.users])
                        } else {
                            NotificationCenter.default.post(name: .UsersDidNotGetted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func search(username: String) {
        queue.async {
            let req = self.reqFactory.userSearchByUsername(accToken: self.accToken, username: username)
            
            do {
                
                _ = try self.service.searchByUsername(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UsersDidSearched, object: self, userInfo: ["users": resp!.users])
                        } else {
                            NotificationCenter.default.post(name: .UsersDidNotSearched, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func edit(name: String) {
        queue.async {
            let req = self.reqFactory.userEditNameReq(accToken: self.accToken, name: name, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.editName(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserNameDidEdited, object: self)
                        } else {
                            NotificationCenter.default.post(name: .UserNameDidNotEdited, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func edit(username: String) {
        queue.async {
            let req = self.reqFactory.userEditUsernameReq(accToken: self.accToken, username: username, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.editUsername(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserUsernameDidEdited, object: self)
                        } else {
                            NotificationCenter.default.post(name: .UserUsernameDidNotEdited, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func edit(phone: Phone) {
        queue.async {
            let req = self.reqFactory.userEditPhoneReq(accToken: self.accToken, phone: phone, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.editPhone(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserPhoneDidEdited, object: self)
                        } else {
                            NotificationCenter.default.post(name: .UserPhoneDidNotEdited, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func edit(oldPassword: String, newPassword: String) {
        queue.async {
            let req = self.reqFactory.userEditPasswordReq(accToken: self.accToken, oldPassword: oldPassword, newPassword: newPassword)
            
            do {
                _ = try self.service.editPassword(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserPasswordDidEdited, object: self)
                        } else {
                            NotificationCenter.default.post(name: .UserPasswordDidNotEdited, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func set(status: Status) {
        queue.async {
            let req = self.reqFactory.userSetStatusReq(accToken: self.accToken, status: status, token: PushCenter.shared.getToken())
            
            do {
                _ = try self.service.setStatus(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserStatusDidChanged, object: self, userInfo: ["status": status])
                        } else {
                            NotificationCenter.default.post(name: .UserStatusDidNotChanged, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func addToken(token: String) {
        queue.async {
            let req = self.reqFactory.userAddTokenReq(accToken: self.accToken, token: token)
            
            do {
                _ = try self.service.addToken(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserTokenDidAdded, object: self)
                        } else {
                            NotificationCenter.default.post(name: .UserTokenDidNotAdded, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func delToken(token: String) {
        queue.async {
            let req = self.reqFactory.userDelTokenReq(accToken: self.accToken, token: token)
            
            do {
                _ = try self.service.delToken(req, completion: { (resp, _) in
                    if !self.isForbidden(with: resp?.code) {
                        if let val = resp?.result.value, val != 0 {
                            NotificationCenter.default.post(name: .UserTokenDidDeleted, object: self)
                        } else {
                            NotificationCenter.default.post(name: .UserTokenDidNotDeleted, object: self)
                        }
                    } else {
                        self.sendForbidden()
                    }
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: Helps
    
    private func sendForbidden() {
        NotificationCenter.default.post(name: .PermissionDenied, object: self)
    }
    
    private func isForbidden(with code: user_StatusCode?) -> Bool {
        return code != nil && code!.value == StatusCodes.PermissionDenied.rawValue
    }
    
}
