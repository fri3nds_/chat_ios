//
//  PushCenter.swift
//  Simple
//
//  Created by Nikita Bondar on 18.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation
import AVFoundation
import UserNotifications

extension Notification.Name {
    static let PushDialogDidAdded       = Notification.Name("PushDialogDidAdded")
    static let PushMessageDidAdded      = Notification.Name("PushMessageDidAdded")
    static let PushLastDidUpdate        = Notification.Name("PushLastDidUpdate")
    static let PushLastDidUpdateFriend  = Notification.Name("PushLastDidUpdateFriend")
}

class PushCenter: NSObject, UNUserNotificationCenterDelegate {
    
    // MARK: Static part
    
    private static var center: PushCenter?
    public static var shared: PushCenter {
        if center != nil {
            return center!
        }
        center = PushCenter()
        return center!
    }
    
    // MARK: Methods
    
    public func getToken() -> String {
        return token
    }
    
    public func setToken(string: String) {
        token = string
    }

    // MARK: Properties
    
    private var token: String = ""
    
    // MARK: User notification center delegate
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        parse(userInfo: notification.request.content.userInfo)
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        parse(userInfo: response.notification.request.content.userInfo)
    }
    
    // MARK: Events
    
    public enum Events: String {
        case PushDialogDidAdded         = "DialogDidAdded"
        case PushMessageDidAdded        = "MessageDidAdded"
        case PushLastDidUpdate          = "LastDidUpdate"
        case PushLastDidUpdateFriend    = "LastDidUpdateFriend"
    }
    
    // MARK: Parser
    
    private func parse(userInfo: [AnyHashable:Any]) {
        if let data = userInfo as? [String:Any], let name = data["name"] as? String {
            print(name)
            switch Events(rawValue: name)! {
            case .PushDialogDidAdded: NotificationCenter.default.post(name: .PushDialogDidAdded, object: self, userInfo: data)
            case .PushMessageDidAdded: NotificationCenter.default.post(name: .PushMessageDidAdded, object: self, userInfo: data)
            case .PushLastDidUpdate: NotificationCenter.default.post(name: .PushLastDidUpdate, object: self, userInfo: data)
            case .PushLastDidUpdateFriend: NotificationCenter.default.post(name: .PushLastDidUpdateFriend, object: self, userInfo: data)
            }
        }
    }
    
}
