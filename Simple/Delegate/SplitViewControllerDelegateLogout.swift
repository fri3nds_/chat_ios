//
//  SplitViewControllerDelegateLogout.swift
//  Simple
//
//  Created by Nikita Bondar on 20.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

protocol SplitViewControllerDelegateLogout {
    func didLogout()
}
