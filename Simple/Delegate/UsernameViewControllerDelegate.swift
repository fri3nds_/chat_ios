//
//  UsernameSearchControllerDelegate.swift
//  Simple
//
//  Created by Nikita Bondar on 30.08.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

protocol UsernameViewControllerDelegate {
    func usernameDidChange(username: String) -> Void
}
