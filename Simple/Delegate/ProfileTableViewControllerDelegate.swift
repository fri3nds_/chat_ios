//
//  ProfileTableViewControllerDelegate.swift
//  Simple
//
//  Created by Nikita Bondar on 31.08.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

protocol ProfileTableViewControllerDelegate {
    func nameDidChange(name: String)
    func phoneDidChange(phone: Phone)
    func usernameDidChange(username: String)
}
