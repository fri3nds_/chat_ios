//
//  ScaleAnimator.swift
//  Simple
//
//  Created by Nikita Bondar on 30.08.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class ScaleAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 0.75
    var presenting = false
    
    var auxAnimations: (() -> Void)?
    var auxAnimationsCancel: (() -> Void)?
    
    var fromSize: CGSize?
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromSize = fromSize else { fatalError("from size is nil") }
        
        let container = transitionContext.containerView
        let to = transitionContext.view(forKey: .to)!
        
        let size = CGSize(width: 200.0, height: 200.0)
        to.frame = CGRect(x: fromSize.width / 2 - size.width / 2, y: 100.0, width: size.width, height: size.height)
        
        container.addSubview(to)
        
        to.transform = CGAffineTransform(scaleX: 1.33, y: 1.33).concatenating(CGAffineTransform(translationX: 0.0, y: 200.0))
        
        //to.alpha = 0.0
        
        let animator = UIViewPropertyAnimator(duration: duration, curve: .easeOut)
        
        animator.addAnimations {
            //to.alpha = 1.0
            to.transform = CGAffineTransform(translationX: 0.0, y: 100.0)
        }
        
        animator.addAnimations {
            self.auxAnimations?()
        }
        
        animator.addCompletion { _ in
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
        
        animator.startAnimation()
    }
    

}
