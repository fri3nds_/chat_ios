//
//  UserManager.swift
//  Simple
//
//  Created by Nikita Bondar on 07.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit
import CoreData

class UserManager {
    
    // MARK: Static
    
    private static var sharedManager: UserManager?
    
    public static var shared: UserManager {
        if let manager = sharedManager {
            return manager
        }
        sharedManager = UserManager(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
        return sharedManager!
    }
    
    // MARK: User
    
    private var helpUser: User?
    
    public var current: User? {
        if let user = helpUser {
            return user
        }
        helpUser = getCurrent()
        return helpUser
    }
    
    public var users: [User] = [] {
        didSet {
            users.sort { $0.name < $1.name }
        }
    }
    
    // MARK: Init
    
    public var context: NSManagedObjectContext
    
    public init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    // MARK: User methods
    
    public func get() -> [User] {
        let request: NSFetchRequest<User> = User.fetchRequest()
        if let users = try? context.fetch(request) as [User] {
            return users
        }
        return []
    }
    
    public func get(id: String) -> User? {
        let request: NSFetchRequest<User> = User.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", id)
        if let users = try? context.fetch(request) as [User] {
            return users.first
        }
        return nil
    }
    
    @discardableResult public func add(user: User) -> Bool {
        do {
            try context.save()
            return true
        } catch {
            print(error.localizedDescription)
        }
        return false
    }
    
    @discardableResult public func save(user: User) -> Bool {
        do {
            try context.save()
            return true
        } catch {
            print(error.localizedDescription)
        }
        return false
    }
    
    @discardableResult public func delete(id: String) -> Bool {
        if let user = get(id: id) {
            if let curr = current, curr.id == user.id {
                helpUser = nil
            }
            context.delete(user)
            do {
                try context.save()
                return true
            } catch {
                print(error.localizedDescription)
            }
        }
        return false
    }
    
    @discardableResult public func deleteAll() -> Bool {
        let request: NSFetchRequest<User> = User.fetchRequest()
        if let users = try? context.fetch(request) as [User], users.count > 0 {
            users.forEach {
                if let curr = self.current, curr.id == $0.id {
                    self.helpUser = nil
                }
                context.delete($0)
            }
            do {
                try context.save()
                return true
            } catch {
                print(error.localizedDescription)
            }
        }
        return false
    }

    // MARK: Helps
    
    private func getCurrent() -> User? {
        let request: NSFetchRequest<User> = User.fetchRequest()
        request.predicate = NSPredicate(format: "curr = %d", 1)
        if let users = try? context.fetch(request) as [User], users.count > 0 {
            return users.first
        }
        return nil
    }
    
}
