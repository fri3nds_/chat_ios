//
//  ContactManager.swift
//  Simple
//
//  Created by Nikita Bondar on 10.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation
import Contacts

extension Notification.Name {
    static let ContactsAccessGranted    = Notification.Name("ContactsAccessGranted")
    static let ContactsAccessDenied     = Notification.Name("ContactsAccessDenied")
    
    static let ContactsDidFetched       = Notification.Name("ContactsDidFetched")
}

class ContactManager {
    
    // MARK: Static
    
    private static var manager: ContactManager?
    public static var shared: ContactManager {
        if let manager = manager {
            return manager
        }
        manager = ContactManager()
        return manager!
    }
    
    // MARK: Properties
    
    private var store: CNContactStore
    private var accessGranted: Bool
    
    public var contacts: [CNContact] = []
    public var sortedContacts: [Contact] = []
    
    private var phoneFactory: PhoneFactory = PhoneFactory()
    
    // MARK: System
    
    public init() {
        store = CNContactStore()
        accessGranted = false
    }
    
    // MARK: Methods
    
    public func request() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.store.requestAccess(for: .contacts) { (granted, error) in
                self.accessGranted = granted
                
                if granted {
                    NotificationCenter.default.post(name: .ContactsAccessGranted, object: self)
                } else {
                    NotificationCenter.default.post(name: .ContactsAccessDenied, object: self)
                }
            }
        }
    }
    
    public func fetch() {
        if self.accessGranted {
            DispatchQueue.global(qos: .userInitiated).sync {
                let keys = [CNContactPhoneNumbersKey, CNContactGivenNameKey, CNContactFamilyNameKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    self.contacts = []
                    self.sortedContacts = []
                    try self.store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                        self.contacts.append(contact)
                        self.sortedContacts.append(Contact(contact: contact))
                    })
                    self.sortedContacts.sort { $0.givenName + $0.familyName < $1.givenName + $1.familyName }
                    NotificationCenter.default.post(name: .ContactsDidFetched, object: self)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    public func phonesFromContacts() -> [Phone] {
        var phones: [Phone] = []
        sortedContacts.forEach { $0.phones.forEach { phones.append(phoneFactory.convert(phone: $0)) }}
        return phones
    }
    
}
