//
//  ChatViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 11.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Message cell"
private let reuseIdentifierFriend = "Friend message cell"

class MessagesViewController: UIViewController {
    
    // MARK: Properties
    
    private var delay: DispatchTime = DispatchTime.now()
    
    private var chatService: ChatService = ChatService.shared
    private var userService: UserService = UserService.shared
    private var messageService: MessageService = MessageService.shared

    public var chat: Chat?
    public var friend: User?
    public var friends: [User]?
    
    private var chatFactory: ChatFactory = ChatFactory()
    private var userFactory: UserFactory = UserFactory()
    private var messageFactory: MessageFactory = MessageFactory()
    
    private var isPlaceholderInEntry: Bool = true
    
    private var forwardMessages: [Message] = []
    private var deferMessages: [Message] = []
    
    private var messages: [Message] = []
    private var messagesPage: Int = 0
    private var pageSize: Int = 30
    
    private var isAddingChat: Bool = false
    
    private var animationCount: Int = 0
    private var animationCurrentCount: Int = 0
    
    private var isAnimatingCollectionView: Bool = false
    private var animationComplete: [(() -> Void)] = []
    
    private var unreadedMessages: [Int] = []
    private var unreadedMessagesFriend: [Int] = []
    
    private var minEntryHeight = 36.0
    private var maxEntryHeight = 93.0
    
    private var controllerTitle: String = ""

    // MARK: Outlets and actions
    
    public var isHiddenScreen: Bool = true
    
    @IBOutlet weak var mainView: UIView! {
        didSet {
            mainView.isHidden = isHiddenScreen
        }
    }
    
    @IBOutlet weak var footer: UIView!
    @IBOutlet weak var footerBottom: NSLayoutConstraint!
    @IBOutlet weak var footerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var entryContainer: UIView! {
        didSet {
            entryContainer.layer.borderColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
        }
    }
    @IBOutlet weak var entryContainerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var entryMessage: UITextView! {
        didSet {
            entryMessage.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            entryMessage.delegate = self
            entryMessage.textContainerInset = .zero
        }
    }
    @IBOutlet weak var entryMessageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sendBtn: UIButton!
    
    @IBAction func send(_ sender: UIButton? = nil) {
        if !entryMessage.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty, let user = UserManager.shared.current {
            let message = messageFactory.new(
                text: entryMessage.text.trimmingCharacters(in: .whitespacesAndNewlines),
                userId: user.id,
                chatId: chat?.id ?? "",
                messages: forwardMessages
            )

            if chat != nil {
                messageService.add(message: message)
                prepand(messages: [message]) { completed in
                    self.scrollMessages(to: 0)
                }
            } else {
                isAddingChat = true
                chatService.addDialog(friend: friend!.id) { ch in
                    message.chatId = ch.id
                    self.messageService.add(message: message)
                }
                prepand(messages: [message]) { completed in
                    self.scrollMessages(to: 0)
                }
            }
            
            entryMessage.text = ""
            growTextView(text: entryMessage.text)
        }
    }
    
    @IBOutlet weak var messagesCollectionView: UICollectionView! {
        didSet {
            messagesCollectionView.transform = CGAffineTransform(rotationAngle: .pi)
        }
    }
    
    // MARK: Screen Pan gesture outlets, actions and helps
    
    @IBOutlet var screenEdgePanGesture: UIScreenEdgePanGestureRecognizer!
    
    @IBAction func handlePan(_ sender: UIScreenEdgePanGestureRecognizer) {
        let progress = min(max(sender.translation(in: messagesCollectionView).x / 50.0, 0.01), 0.99)
        
        switch sender.state {
        case .began:
            isAnimatingCollectionView = true
            pausedTime = CACurrentMediaTime()
            showTimeCells(progress: 0.0)
            
        case .changed:
            showTimeCells(progress: progress)
            
        case .cancelled, .ended:
            sender.isEnabled = false
            showTimeCells(progress: progress)
            pausedTime = CACurrentMediaTime()
            hideTimeCells(progress: progress)
            
        default: break
        }
    }
    
    private var pausedTime: CFTimeInterval!
    private var duration: CFTimeInterval = 1.0
    private var offset: CGFloat = 50.0
    private var positionX: CGFloat = 0.0
    private var timeInitialTrailing: CGFloat = -40.0
    
    private func showTimeCells(progress: CGFloat) {
        messagesCollectionView.visibleCells.forEach { cell in
            if let cell = cell as? FriendMessageCell {
                cell.timeTrailing.constant = timeInitialTrailing + progress * offset
            }
            if let cell = cell as? MessageCell {
                cell.showTime?(progress, cell.layer)
            }
        }
    }
    
    private func hideTimeCells(progress: CGFloat) {
        messagesCollectionView.visibleCells.forEach { cell in
            if let cell = cell as? FriendMessageCell {
                self.hideTimeFriend(progress, cell: cell)
            }
            if let cell = cell as? MessageCell {
                cell.hideTime?(progress, cell.layer)
            }
        }
    }
    
    // MARK: Tap gesture outlets and actions
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    @IBAction func openKeyboard(_ sender: UITapGestureRecognizer) {
        entryMessage.becomeFirstResponder()
    }
    
    // MARK: Empty screen outlets
    
    @IBOutlet weak var simpleTitle: UILabel!
    
    // MARK: Controller methods
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { _ in
            self.messagesCollectionView.collectionViewLayout.invalidateLayout()
            self.growTextView(text: self.entryMessage.text)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: Setups
    
    public func setup() {
        navigationItem.title = ""
        
        if let nav = navigationController, nav.navigationBar.isUpdating {
            navigationController?.navigationBar.showUpdating(value: false)
            navigationItem.showUpdating(value: true)
        }
        
        navigationItem.showUpdating(value: !isHiddenScreen)
        mainView.isHidden = isHiddenScreen
        
        if !isHiddenScreen {
            setAvatar(image: nil)
        }
        
        if let ch = chat, let user = UserManager.shared.current {
            messageService.get(cid: ch.id, page: incPage())
            
            let index = user.id == ch.users[0] ? 1 : 0
            if let friend = UserManager.shared.get(id: ch.users[index]) {
                self.friend = friend
            } else {
                userService.get(id: ch.users[index])
            }
            
        } else if let us = friend {
            chatService.get(with: us.id)
        }
        
    }
    
    public func clean() {
        chat = nil
        friend = nil
        friends = nil
        messagesPage = 0
        unreadedMessages = []
        unreadedMessagesFriend = []
        screenEdgePanGesture.isEnabled = true
        entryMessage.text = ""
        entryMessage.placeholder(text: "Сообщение...", color: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1))
        view.endEditing(true)
        messages = []
        messagesCollectionView.reloadData()
    }
    
    // MARK: Chat methods
    
    private func updateLastActions() {
        if let chat = self.chat {
            chatService.updateLast(chatId: chat.id)
        }
    }
    
    // MARK: Message methods
    
    private func append(messages: [Message], completion: ((Bool) -> Void)? = nil) {
        let call = {
            self.messagesCollectionView.performBatchUpdates({
                var last = self.messages.count
                var indexes: [IndexPath] = []
                messages.forEach { self.messages.insert($0, at: last); indexes.append(IndexPath(row: last, section: 0)); last += 1 }
                
                self.messagesCollectionView.insertItems(at: indexes)
            }, completion: {
                completion?($0)
            })
        }
        
        if isAnimatingCollectionView {
            animationComplete.append { call() }
        } else {
            call()
        }
    }
    
    private func prepand(messages: [Message], completion: ((Bool) -> Void)? = nil) {
        let call = {
            self.incUnreadedIndexes(byValue: messages.count)
            self.messagesCollectionView.performBatchUpdates({
                var first = 0
                var indexes: [IndexPath] = []
                messages.forEach {
                    self.messages.insert($0, at: first)
                    indexes.append(IndexPath(row: first, section: 0))
                    first += 1
                }
                
                self.messagesCollectionView.insertItems(at: indexes)
            }) {
                completion?($0)
                self.scrollMessages(to: 0)
            }
        }
        
        if isAnimatingCollectionView {
            animationComplete.append { call() }
        } else {
            call()
        }
    }
    
    private func findLastReaded() -> Int {
        for (i, _) in messages.enumerated() {
            if isReaded(index: i, ignoreMine: true) {
                return i
            }
        }
        return -1
    }
    
    private func isReaded(index: Int, ignoreMine: Bool = false) -> Bool {
        if let chat = self.chat, let user = UserManager.shared.current {
            if ignoreMine {
                return messages[index].userId == user.id || chat.lastUpdate > 0 && messages[index].date <= chat.lastUpdate
            } else {
                let isMine = messages[index].userId == user.id
                return isMine
                    ? chat.lastUpdateFriend > 0 && messages[index].date <= chat.lastUpdateFriend
                    : chat.lastUpdate > 0 && messages[index].date <= chat.lastUpdate
            }
        }
        
        return false
    }
    
    private func sortMessages() {
        self.messages.sort { $0.date > $1.date }
    }
    
    private func scrollMessages(to index: Int) {
        if index > 0 {
            let indexPath = IndexPath(row: index, section: 0)
            messagesCollectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    private func updateMessage(date: Int, indexPath: IndexPath) {
        if let cell = messagesCollectionView.cellForItem(at: indexPath) {
            if let cell = cell as? FriendMessageCell {
                cell.update(date: date)
            }
            if let cell = cell as? MessageCell {
                cell.update(date: date)
            }
        }
    }
    
    // MARK: Readed/Unreaded message methods
    
    private func reloadUnreaded(byFriend: Bool) {
        if let chat = self.chat {
            let messages = byFriend ? unreadedMessagesFriend : unreadedMessages
            let lastUpdate = byFriend ? chat.lastUpdateFriend : chat.lastUpdate
            let indexes = messages.filter { self.messages[$0].date <= lastUpdate }
            let newUnreadedArray = messages.filter { self.messages[$0].date > lastUpdate }
            
            if byFriend {
                unreadedMessagesFriend = newUnreadedArray
            } else {
                unreadedMessages = newUnreadedArray
            }
            
            let indexPaths = indexes.map { IndexPath(row: $0, section: 0) }
            if isAnimatingCollectionView {
                animationComplete.append { self.messagesCollectionView.reloadItems(at: indexPaths) }
            } else {
                messagesCollectionView.reloadItems(at: indexPaths)
            }
            
            print(indexes)
        }
    }
    
    private func incUnreadedIndexes(byValue: Int) {
        unreadedMessages = unreadedMessages.map { $0 + byValue }
        unreadedMessagesFriend = unreadedMessagesFriend.map { $0 + byValue }
    }
    
    // MARK: Text view methods
    
    private func growTextView(text: String) {
        let lineHeight = entryMessage.font!.lineHeight
        let minHeight: CGFloat = 20.0
        let minHeightContainer: CGFloat = 36.0
        let footHeight: CGFloat = 50.0
        let maxHeight: CGFloat = 100.0
        let size = entryMessage.estimatedSize(forText: text)
        let koef = Int(size.height / lineHeight) - 1
        let newHeight =  lineHeight * CGFloat(koef)
        
        if newHeight < maxHeight && newHeight != entryMessageHeight.constant {
            entryMessageHeight.constant = minHeight + newHeight
            entryContainerHeight.constant = minHeightContainer + newHeight
            footerHeight.constant = footHeight + lineHeight * CGFloat(koef)
            UIView.animate(withDuration: 0.1, animations: {
                self.footer.layoutIfNeeded()
            })
        }
    }
    
    // MARK: Help methods
    
    private func setAvatar(image: UIImage?) {
        if let nav = navigationController {
            let view = UIButton(frame: CGRect(x: nav.navigationBar.bounds.width - 46, y: 4, width: 36, height: 36))
            view.tag = 15
            view.rounded()
            view.setImage(image, for: .normal)
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: view)
        }
    }
    
    private func updateTitle() {
        controllerTitle = self.friend?.name ?? self.chat?.title ?? ""
        
        let fade = CATransition()
        fade.duration = 0.25
        fade.type = kCATransitionFade
        navigationController?.navigationBar.layer.add(fade, forKey: "fade")
        
        delayTo(seconds: 0.25) {
            let fade = CATransition()
            fade.duration = 0.25
            fade.type = kCATransitionFade
            self.navigationController?.navigationBar.layer.add(fade, forKey: "fade")
            
            self.navigationItem.show(title: self.controllerTitle, with: self.friend?.status)
        }
    }
    
    private func incDelay(to seconds: Double) {
        delay = DispatchTime.now() + seconds
    }
    
    private func animateDelay() -> DispatchTime {
        let now = DispatchTime.now()
        if now.rawValue > delay.rawValue {
            return now
        }
        return delay
    }
    
    private func incPage() -> Int {
        let page = messagesPage
        messagesPage += 1
        return page
    }

}

extension MessagesViewController: UITextViewDelegate {
    
    // MARK: Text view delegate
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) {
            entryMessage.placeholder(text: "", color: .black)
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            entryMessage.placeholder(text: "Сообщение...", color: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1))
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newText = (text.count > 0 ? textView.text + text : textView.text) as NSString
        if text.count == 0 {
            newText = newText.replacingCharacters(in: range, with: "") as NSString
        }
        growTextView(text: String(newText))
        return true
    }
    
}

extension MessagesViewController: UICollectionViewDelegate {
    
    // MARK: Collection view delegate
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        view.endEditing(true)
        if entryMessage.textColor == .black, entryMessage.text.isEmpty {
            entryMessage.placeholder(text: "Сообщение...", color: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1))
        }
    }
    
}

extension MessagesViewController: UICollectionViewDataSource {
    
    // MARK: Collection view data source
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let user = UserManager.shared.current else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        }
        
        let isMine = user.id == messages[indexPath.row].userId
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: isMine ? reuseIdentifier : reuseIdentifierFriend, for: indexPath)
        
        // plus messages
        if let chat = chat, indexPath.row == messages.count - 5 && messages.count == messagesPage * pageSize {
            messageService.get(cid: chat.id, page: incPage())
        }
        
        let isReaded = self.isReaded(index: indexPath.row)
        
        if isMine, let cell = cell as? MessageCell {
            cell.isReaded = isReaded
            cell.messageView.text = messages[indexPath.row].text
            
            cell.update(date: messages[indexPath.row].date)
            cell.update(size: CGSize(width: messagesCollectionView.frame.width, height: 5000))
            
            cell.showTime = showTime(_:layer:)
            cell.hideTime = hideTime(_:layer:)
            
            if !isReaded, !unreadedMessagesFriend.contains(indexPath.row) {
                unreadedMessagesFriend.append(indexPath.row)
            }
            
        } else if let cell = cell as? FriendMessageCell {
            cell.isReaded = isReaded
            cell.messageView.text = messages[indexPath.row].text
            
            cell.update(date: messages[indexPath.row].date)
            cell.update(size: CGSize(width: messagesCollectionView.frame.width, height: 5000))
            
            if !isReaded, !unreadedMessages.contains(indexPath.row) {
                unreadedMessages.append(indexPath.row)
            }
            
        }
        
        return cell
    }
    
}

extension MessagesViewController: UICollectionViewDelegateFlowLayout {
    
    // MARK: Collection view flow layout delegate
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = messages[indexPath.item].text
        let size = CGSize(width: messagesCollectionView.bounds.width * 0.7, height: 5000)
        
        let textSize = entryMessage.font!.pointSize
        let estimated = NSString(string: text).boundingRect(
            with: size,
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.systemFont(ofSize: textSize)],
            context: nil
        )
        
        return CGSize(width: collectionView.frame.width - 1, height: estimated.height + 20 + 4)
    }
    
}

extension MessagesViewController: UIGestureRecognizerDelegate {
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
}

extension MessagesViewController {
    
    // MARK: Collection view animate methods
    
    private func showTime(_ progress: CGFloat, layer: CALayer) {
        if layer.animation(forKey: "move") == nil {
            positionX = layer.position.x
            layer.beginTime = pausedTime
            
            let anim = CABasicAnimation(keyPath: "position.x")
            anim.duration = duration
            anim.fromValue = layer.position.x
            anim.toValue = layer.position.x + offset
            
            layer.timeOffset = pausedTime
            layer.speed = 0.0
            layer.add(anim, forKey: "move")
        } else {
            layer.timeOffset = pausedTime + TimeInterval(duration) * TimeInterval(progress)
        }
        layer.position.x = positionX + offset * progress
    }
    
    private func hideTime(_ progress: CGFloat, layer: CALayer) {
        layer.removeAnimation(forKey: "move")
        layer.beginTime = pausedTime
        
        let anim = CABasicAnimation(keyPath: "position.x")
        anim.duration = 0.4
        anim.fromValue = layer.position.x
        anim.toValue = layer.position.x - offset * progress
        anim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        anim.delegate = self
        animationCount += 1
        
        layer.speed = 1.0
        layer.add(anim, forKey: "move")
        layer.position.x = layer.position.x - offset * progress
    }
    
    private func hideTimeFriend(_ progress: CGFloat, cell: UICollectionViewCell) {
        if let cell = cell as? FriendMessageCell {
            animationCount += 1
            cell.timeTrailing.constant = timeInitialTrailing
            UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {
                cell.layoutIfNeeded()
            }, completion: { _ in self.animationDidStop() })
        }
    }
    
    private func animationDidStop() {
        animationCurrentCount += 1
        if animationCurrentCount == animationCount {
            isAnimatingCollectionView = false
            screenEdgePanGesture.isEnabled = true
            animationCount = 0
            animationCurrentCount = 0
            animationComplete.forEach { $0() }
            animationComplete.removeAll()
        }
    }
    
}

extension MessagesViewController: CAAnimationDelegate {
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        animationDidStop()
    }
    
}

extension MessagesViewController {
    
    // MARK: Register/remove observers
    
    public func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillChangeFrame, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
        center.addObserver(self, selector: #selector(pushMessageDidAdded(notification:)), name: .PushMessageDidAdded, object: PushCenter.shared)
        center.addObserver(self, selector: #selector(pushLastDidUpdateFriend(notification:)), name: .PushLastDidUpdateFriend, object: PushCenter.shared)
        
        center.addObserver(self, selector: #selector(didBecomeActive(notification:)), name: .UIApplicationDidBecomeActive, object: nil)
        
        center.addObserver(self, selector: #selector(networkIsUnavailable(notification:)), name: .NetworkIsUnavailable, object: nil)
        center.addObserver(self, selector: #selector(networkDidAvailable(notification:)), name: .NetworkDidAvailable, object: nil)
        
        center.addObserver(self, selector: #selector(chatDidGetted(notification:)), name: .ChatDidGetted, object: chatService)
        center.addObserver(self, selector: #selector(chatDidNotGetted(notification:)), name: .ChatDidNotGetted, object: chatService)
        
        center.addObserver(self, selector: #selector(chatDidAdded(notification:)), name: .ChatDidAdded, object: chatService)
        center.addObserver(self, selector: #selector(chatDidNotAdded(notification:)), name: .ChatDidNotAdded, object: chatService)
        
        center.addObserver(self, selector: #selector(chatLastDidUpdate(notification:)), name: .ChatLastDidUpdate, object: chatService)
        center.addObserver(self, selector: #selector(chatLastDidNotUpdate(notification:)), name: .ChatLastDidNotUpdate, object: chatService)
        
        center.addObserver(self, selector: #selector(userDidGetted(notification:)), name: .UserDidGetted, object: userService)
        center.addObserver(self, selector: #selector(userDidNotGetted(notification:)), name: .UserDidNotGetted, object: userService)
        
        center.addObserver(self, selector: #selector(messageDidGetted(notification:)), name: .MessageDidGetted, object: messageService)
        center.addObserver(self, selector: #selector(messageDidNotGetted(notification:)), name: .MessageDidNotGetted, object: messageService)
        
        center.addObserver(self, selector: #selector(messagesDidGetted(notification:)), name: .MessagesDidGetted, object: messageService)
        center.addObserver(self, selector: #selector(messagesDidNotGetted(notification:)), name: .MessagesDidNotGetted, object: messageService)
        
        center.addObserver(self, selector: #selector(messagesDidGettedUpdates(notification:)), name: .MessagesDidGettedUpdates, object: messageService)
        center.addObserver(self, selector: #selector(messagesDidNotGettedUpdates(notification:)), name: .MessagesDidNotGettedUpdates, object: messageService)
        
        center.addObserver(self, selector: #selector(messageDidAdded(notification:)), name: .MessageDidAdded, object: messageService)
        center.addObserver(self, selector: #selector(messageDidNotAdded(notification:)), name: .MessageDidNotAdded, object: messageService)
        
        center.addObserver(self, selector: #selector(refreshDidDone(notification:)), name: .RefreshDidDone, object: nil)
    }
    
    public func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension MessagesViewController {
    
    // MARK: Keyboard notifications
    
    @objc private func keyboardWillShow(notification: Notification) {
        if let userInfo = notification.userInfo, let keyboardRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue  {
            if #available(iOS 11.0, *) {
                footerBottom.constant = keyboardRect.height - view.safeAreaInsets.bottom
            } else {
                footerBottom.constant = keyboardRect.height
            }
            UIView.animate(withDuration: 0, animations: {
                self.view.layoutIfNeeded()
            }, completion: { completed in
                self.scrollMessages(to: 0)
            })
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        footerBottom.constant = 0
        UIView.animate(withDuration: 0, animations: {
            self.view.layoutIfNeeded()
        }, completion: { completed in
            self.scrollMessages(to: 0)
        })
    }
    
}

extension MessagesViewController {
    
    // MARK: Chat notifications
    
    @objc private func chatDidGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            if let ch = notification.userInfo!["chat"] as? chat_Chat {
                let chat = self.chatFactory.convert(chat: ch)
                self.chat = chat
                self.navigationItem.showUpdating(value: true)
                self.messageService.get(cid: chat.id, page: self.incPage())
            }
        }
    }
    
    @objc private func chatDidNotGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            print("Ooops, chat did not getted")
            self.navigationItem.showUpdating(value: false, completion: { self.updateTitle() })
        }
    }
    
    @objc private func chatDidAdded(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            if self.isAddingChat, let ch = notification.userInfo!["chat"] as? chat_Chat {
                self.isAddingChat = false
                
                let chat = self.chatFactory.convert(chat: ch)
                self.chat = chat
            }
        }
    }
    
    @objc private func chatDidNotAdded(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            self.isAddingChat = false
            print("Ooops, chat did not added")
        }
    }
    
    @objc private func chatLastDidUpdate(notification: Notification) {
        DispatchQueue.main.async {
            if let chat = self.chat, let date = notification.userInfo!["date"] as? Int64 {
                print(date)
                chat.lastUpdate = Int(date)
                if self.unreadedMessages.count > 0 {
                    self.reloadUnreaded(byFriend: false)
                }
            }
        }
    }
    
    @objc private func chatLastDidNotUpdate(notification: Notification) {
        DispatchQueue.main.async {
            print("Ooops, last did not updated")
        }
    }
    
}

extension MessagesViewController {
    
    // MARL: User notifications
    
    @objc private func userDidGetted(notification: Notification) {
        DispatchQueue.main.async {
            if let us = notification.userInfo!["user"] as? user_User, let chat = self.chat, chat.users.contains(us.id) {
                let user = self.userFactory.convert(uUser: us)
                self.friend = user
                self.navigationItem.show(title: self.controllerTitle, with: user.status)
            }
        }
    }
    
    @objc private func userDidNotGetted(notification: Notification) {
        DispatchQueue.main.async {
            self.navigationItem.show(title: self.controllerTitle)
            print("Ooops, user did not getted")
        }
    }
    
}

extension MessagesViewController {
    
    // MARK: Message notifications
    
    @objc private func messageDidGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            if let mes = notification.userInfo!["message"] as? message_Message {
                let message = self.messageFactory.convert(message: mes)
                self.prepand(messages: [message])
            }
        }
    }
    
    @objc private func messageDidNotGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            print("Ooops, message did not getted")
        }
    }
    
    @objc private func messagesDidGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            self.navigationItem.showUpdating(value: false, completion: { self.updateTitle() })
            
            if let mess = notification.userInfo!["messages"] as? [message_Message], mess.count > 0 {
                let messages = self.messageFactory.convert(messages: mess)
                self.append(messages: messages, completion: { _ in
                    self.scrollMessages(to: self.findLastReaded())
                })
            }
            
            delayTo(seconds: 1.0, completion: { self.updateLastActions() })
        }
    }
    
    @objc private func messagesDidNotGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            self.navigationItem.showUpdating(value: false, completion: { self.updateTitle() })
            print("Ooops, messages did not getted")
        }
    }
    
    @objc private func messageDidAdded(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            if let id = notification.userInfo!["id"] as? message_Id, let date = notification.userInfo!["date"] as? Int64, let message = notification.userInfo!["message"] as? Message {
                if let index = self.messages.index(where: { $0.id == message.id }) {
                    self.messages[index].id = id.value
                    self.messages[index].date = Int(date)
                    
                    let indexPath = IndexPath(row: Int(index), section: 0)
                    self.updateMessage(date: Int(date), indexPath: indexPath)
                }
            }
        }
    }
    
    @objc private func messageDidNotAdded(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            print("Ooops, message did not added")
        }
    }
    
    @objc private func messagesDidGettedUpdates(notification: Notification) {
        DispatchQueue.main.async {
            self.navigationItem.showUpdating(value: false, completion: { self.updateTitle() })
            
            if let mess = notification.userInfo!["messages"] as? [message_Message], mess.count > 0 {
                let messages = self.messageFactory.convert(messages: mess)
                self.prepand(messages: messages)
                
                delayTo(seconds: 1.0, completion: { self.updateLastActions() })
            }
        }
    }
    
    @objc private func messagesDidNotGettedUpdates(notification: Notification) {
        DispatchQueue.main.async {
            self.navigationItem.showUpdating(value: false, completion: { self.updateTitle() })
            print("no updates")
        }
    }
    
}

extension MessagesViewController {
    
    // MARK: Push notifications
    
    @objc private func pushMessageDidAdded(notification: Notification) {
        DispatchQueue.main.async {
            if let chat = self.chat, let mes = notification.userInfo!["message"] as? [String:Any], let chatId = mes["chatId"] as? String, chatId == chat.id {
                let message = self.messageFactory.convert(from: mes)
                self.prepand(messages: [message])
                delayTo(seconds: 1.0, completion: { self.updateLastActions() })
            }
        }
    }
    
    @objc private func pushLastDidUpdateFriend(notification: Notification) {
        DispatchQueue.main.async {
            if let chat = self.chat, let last = notification.userInfo!["last"] as? [String:Any], let chatId = last["chatId"] as? String, chat.id == chatId {
                let date = Int(last["date"] as! Int64)
                chat.lastUpdateFriend = date
                
                if self.unreadedMessagesFriend.count > 0 {
                    self.reloadUnreaded(byFriend: true)
                }
            }
        }
    }
    
}

extension MessagesViewController {
    
    // MARK: Other notifications
    
    @objc private func refreshDidDone(notification: Notification) {
        DispatchQueue.main.async {
            self.setup()
        }
    }
    
    @objc private func didBecomeActive(notification: Notification) {
        if !isHiddenScreen {
            
            if let ch = chat {
                navigationItem.showUpdating(value: true)
                if messages.count > 0 {
                    messageService.getUpdates(chatId: ch.id, date: messages[0].date)
                }
            } else {
                if let friend = friend {
                    navigationItem.showUpdating(value: true)
                    chatService.get(with: friend.id)
                }
            }
        }
    }
    
}

extension MessagesViewController {
    
    // MARK: Network notifications
    
    @objc private func networkIsUnavailable(notification: Notification) {
        if let nav = navigationController, nav.navigationBar.isUpdating {
            nav.navigationBar.showUpdating(value: false)
        }
        if !navigationItem.isUpdating {
            navigationItem.showUpdating(value: !isHiddenScreen)
        }
    }
    
    @objc private func networkDidAvailable(notification: Notification) {
        didBecomeActive(notification: notification)
    }
    
}
