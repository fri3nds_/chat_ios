//
//  PhoneViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 31.08.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class PhoneViewController: UIViewController {
    
    // MARK: Delegate
    
    public var delegate: PhoneViewControllerDelegate?
    
    // MARK: Properties
    
    private var doneBtn: UIBarButtonItem!
    private var userService = UserService.shared
    private var validator = Validator()
    
    // MARK: Outlets and actions
    
    @IBOutlet weak var customNavigationItem: UINavigationItem!
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        presentingViewController?.dismiss(animated: true)
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        doneBtn.showCircle()
        view.endEditing(true)
        phone.isUserInteractionEnabled = false
        userService.edit(phone: Phone(number: "7" + phone.text!))
    }
    
    @IBOutlet weak var phone: UITextField! {
        didSet {
            if let user = UserManager.shared.current {
                phone.text = user.phone.number.after(character: 0)
            }
        }
    }
    
    // MARK: Controller methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateBarButton()
        doneBtn.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Methods
    
    private func updateBarButton() {
        customNavigationItem.rightBarButtonItem = nil
        let barButton = UIBarButtonItem(title: "Готово", style: .done, target: self, action: #selector(done(_:)))
        doneBtn = barButton
        customNavigationItem.rightBarButtonItem = barButton
    }

}

extension PhoneViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty, doneBtn.isEnabled {
            done(doneBtn)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, text.count == 10, string.count != 0 {
            return false
        }
        return true
    }
    
}

extension PhoneViewController {
    
    // MARK: Register/remove observers
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(textFieldDidChange(notification:)), name: .UITextFieldTextDidChange, object: phone)
        
        center.addObserver(self, selector: #selector(phoneDidEdited(notification:)), name: .UserPhoneDidEdited, object: userService)
        center.addObserver(self, selector: #selector(phoneDidNotEdited(notification:)), name: .UserPhoneDidNotEdited, object: userService)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension PhoneViewController {
    
    // MARK: Text field notifications
    
    @objc private func textFieldDidChange(notification: Notification) {
        if let textField = notification.object as? UITextField, let text = textField.text, let user = UserManager.shared.current {
            doneBtn.isEnabled = validator.isPhone(number: text) && text != user.phone.number
        }
    }
    
}

extension PhoneViewController {
    
    // MARK: User notifications
    
    @objc private func phoneDidEdited(notification: Notification) {
        DispatchQueue.main.async {
            if let user = UserManager.shared.current {
                user.phone = Phone(number: "7" + self.phone.text!)
                UserManager.shared.save(user: user)
                self.presentingViewController?.dismiss(animated: true, completion: {
                    self.delegate?.phoneDidChange(phone: user.phone)
                })
            }
        }
    }
    
    @objc private func phoneDidNotEdited(notification: Notification) {
        DispatchQueue.main.async {
            self.phone.isUserInteractionEnabled = true
            self.phone.becomeFirstResponder()
            self.updateBarButton()
        }
    }
    
}
