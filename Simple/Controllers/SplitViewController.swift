//
//  SplitViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 10.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit
import CoreData

extension Notification.Name {
    static let RefreshDidDone = Notification.Name("RefreshDidDone")
    static let NetworkIsUnavailable = Notification.Name("NetworkIsUnavailable")
    static let NetworkDidAvailable = Notification.Name("NetworkDidAvailable")
}

class SplitViewController: UISplitViewController {
    
    // MARK: Delegate
    
    public var delegateLogout: SplitViewControllerDelegateLogout?
    
    // MARK: Properties
    
    public var authService: AuthService?
    
    private var userFactory = UserFactory()
    private var timer: Timer?
    
    // MARK: Controller methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        registerObservers()
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    // MARK: Setups
    
    private func setup() {
        delegate = self
        preferredDisplayMode = .allVisible
        
        Reachability.internetDidAvailable = { NotificationCenter.default.post(name: .NetworkDidAvailable, object: self) }
        
        checkNetwork()
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(checkNetwork), userInfo: nil, repeats: true)
        
        if let user = UserManager.shared.current {
            UserService.shared.get(id: user.id, completion: { user in
                DispatchQueue.main.async {
                    UIApplication.shared.applicationIconBadgeNumber = Int(user.badgeCount)
                }
            })
        }
    }
    
    private func logout() {
        UIApplication.shared.applicationIconBadgeNumber = 0
        UserManager.shared.deleteAll()
        presentingViewController?.dismiss(animated: true, completion: {
            self.delegateLogout?.didLogout()
        })
    }
    
    // MARK: Methods
    
    @objc private func checkNetwork() {
        if !Reachability.isConnectedToNetwork() {
            NotificationCenter.default.post(name: .NetworkIsUnavailable, object: self)
        }
    }

}

extension SplitViewController {
    
    // MARK: Register/remove observers
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(permissionDenied(notification:)), name: .PermissionDenied, object: nil)
        
        center.addObserver(self, selector: #selector(updateUser(notification:)), name: .UserDidGetted, object: UserService.shared)
        
        center.addObserver(self, selector: #selector(didRefreshed(notification:)), name: .DidRefreshed, object: authService)
        center.addObserver(self, selector: #selector(didNotRefreshed(notification:)), name: .DidNotRefreshed, object: authService)
        center.addObserver(self, selector: #selector(invalidToken(notification:)), name: .InvalidToken, object: authService)
        
        center.addObserver(self, selector: #selector(didBecomeActive(notification:)), name: .UIApplicationDidBecomeActive, object: nil)
        center.addObserver(self, selector: #selector(willResignActive(notification:)), name: .UIApplicationWillResignActive, object: nil)
        
        center.addObserver(self, selector: #selector(didLogouted(notification:)), name: .DidLogouted, object: authService)
    }
    
    private func removeObservers() {
        let center = NotificationCenter.default
        center.removeObserver(self, name: .PermissionDenied, object: nil)
        
        center.removeObserver(self, name: .UserDidGetted, object: UserService.shared)
        
        center.removeObserver(self, name: .DidRefreshed, object: authService)
        center.removeObserver(self, name: .DidNotRefreshed, object: authService)
        center.removeObserver(self, name: .InvalidToken, object: authService)
        
        center.removeObserver(self, name: .UIApplicationDidBecomeActive, object: nil)
        center.removeObserver(self, name: .UIApplicationWillResignActive, object: nil)
        
        center.removeObserver(self, name: .DidLogouted, object: authService)
    }
    
}

extension SplitViewController: UISplitViewControllerDelegate {
    
    // MARK: Split view controller delegate
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
    
}

extension SplitViewController {
    
    // MARK: User notifications
    
    @objc private func updateUser(notification: Notification) {
        DispatchQueue.main.async {
            if let user = notification.userInfo!["user"] as? user_User, let curr = UserManager.shared.current, user.id == curr.id {
                let us = self.userFactory.convert(uUser: user)
                UserManager.shared.save(user: us)
            }
        }
    }
    
}

extension SplitViewController {
    
    // MARK: Refresh token
    
    @objc private func permissionDenied(notification: Notification) {
        authService?.refresh()
    }
    
    @objc private func didRefreshed(notification: Notification) {
        DispatchQueue.main.async {
            if let user = UserManager.shared.current, let accToken = notification.userInfo!["accToken"] as? String, let refToken = notification.userInfo!["refToken"] as? String {
                user.accToken = accToken
                user.refToken = refToken
                UserManager.shared.save(user: user)
                NotificationCenter.default.post(name: .RefreshDidDone, object: nil)
            }
        }
    }
    
    @objc private func didNotRefreshed(notification: Notification) {
        DispatchQueue.main.async {
            self.logout()
        }
    }
    
    @objc private func invalidToken(notification: Notification) {
        DispatchQueue.main.async {
            self.logout()
        }
    }
    
}

extension SplitViewController {
    
    // MARK: Active
    
    @objc private func didBecomeActive(notification: Notification) {
        UserService.shared.set(status: Status(value: .online, date: Date().millisecondsSince1970))
    }
    
    @objc private func willResignActive(notification: Notification) {
        UserService.shared.set(status: Status(value: .offline, date: Date().millisecondsSince1970))
    }
    
}

extension SplitViewController {
    
    // MARK: Logout
    
    @objc private func didLogouted(notification: Notification) {
        removeObservers()
        DispatchQueue.main.async {
            self.logout()
        }
    }
    
}
