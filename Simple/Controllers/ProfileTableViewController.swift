//
//  ProfileTableViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 25.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {
    
    // MARK: Delegate
    
    public var delegate: ProfileTableViewControllerDelegate?
    
    // MARK: Properties
    
    private var authService: AuthService = AuthService.shared
    private var userService: UserService = UserService.shared
    
    // MARK: Outlets and actions
    
    @IBOutlet weak var avatar: UIImageView! {
        didSet {
            avatar.rounded()
        }
    }
    
    @IBOutlet weak var givenName: UITextField! {
        didSet {
            if let name = UserManager.shared.current?.name, name.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
                givenName.text = String(name.split(separator: " ")[0])
            }
        }
    }
    
    @IBOutlet weak var familyName: UITextField! {
        didSet {
            if let name = UserManager.shared.current?.name, name.trimmingCharacters(in: .whitespacesAndNewlines).count > 0  {
                let names = name.split(separator: " ")
                if names.count > 1 {
                    familyName.text = String(names[1])
                }
            }
        }
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        guard let user = UserManager.shared.current else { return }
        
        view.endEditing(true)
        sender.showLoading()
        
        let changedName = givenName.text!.trimmingCharacters(in: .whitespacesAndNewlines) + " " + familyName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if changedName != user.name {
            user.name = changedName
            UserManager.shared.save(user: user)
            userService.edit(name: changedName)
        } else {
            updateBarButton()
        }
    }
    
    @IBOutlet weak var number: UILabel! {
        didSet {
            if let user = UserManager.shared.current {
                number.text = user.phone.number
            }
        }
    }
    
    @IBOutlet weak var username: UILabel! {
        didSet {
            if let user = UserManager.shared.current {
                username.text = "@\(user.username)"
            }
        }
    }
    
    // MARK: Controller methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        setup()
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Change phone" {
            if let vc = segue.destination as? PhoneViewController {
                vc.delegate = self
            }
        } else if segue.identifier == "Change username" {
            if let vc = segue.destination as? UsernameViewController {
                vc.delegate = self
            }
        }
    }
    
    // MARK: Setups
    
    private func setup() {
        
    }
    
    // MARK: Methods
    
    private func updateBarButton() {
        let barButton = UIBarButtonItem(title: "Готово", style: .done, target: self, action: #selector(done(_:)))
        navigationItem.rightBarButtonItem = barButton
    }
    
}

extension ProfileTableViewController: PhoneViewControllerDelegate {
    
    public func phoneDidChange(phone: Phone) {
        UIView.transition(with: self.number, duration: 0.25, options: .transitionCrossDissolve, animations: {
            self.number.text = phone.number
            self.delegate?.phoneDidChange(phone: phone)
        })
    }
    
}

extension ProfileTableViewController: UsernameViewControllerDelegate {
    
    public func usernameDidChange(username: String) {
        UIView.transition(with: self.username, duration: 0.25, options: .transitionCrossDissolve, animations: {
            self.username.text = "@\(username)"
            self.delegate?.usernameDidChange(username: username)
        })
    }
    
}

extension ProfileTableViewController {
    
    // MARK: Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 2: performSegue(withIdentifier: "Change phone", sender: nil)
        case 3: performSegue(withIdentifier: "Change username", sender: nil)
        case 4: performSegue(withIdentifier: "Change password", sender: nil)
        case 6: authService.logout()
        default: break
        }
        
    }
    
}

extension ProfileTableViewController: UITextFieldDelegate {
    
    // MARK: Text field delegate
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == givenName {
            familyName.becomeFirstResponder()
        } else {
            if let button = navigationItem.rightBarButtonItem {
                print("done")
                done(button)
            } else {
                view.endEditing(true)
            }
        }
        return false
    }
    
}

extension ProfileTableViewController {
    
    // MARK: Register/remove observers
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(userNameDidEdited(notification:)), name: .UserNameDidEdited, object: userService)
        center.addObserver(self, selector: #selector(userNameDidNotEdited(notification:)), name: .UserNameDidNotEdited, object: userService)
        
        center.addObserver(self, selector: #selector(userPhoneDidEdited(notification:)), name: .UserPhoneDidEdited, object: userService)
        center.addObserver(self, selector: #selector(userPhoneDidNotEdited(notification:)), name: .UserPhoneDidNotEdited, object: userService)
    }
    
    private func removeObservers() {
        let center = NotificationCenter.default
        center.removeObserver(self, name: .UserNameDidEdited, object: userService)
        center.removeObserver(self, name: .UserNameDidNotEdited, object: userService)
        
        center.removeObserver(self, name: .UserPhoneDidEdited, object: userService)
        center.removeObserver(self, name: .UserPhoneDidNotEdited, object: userService)
    }
    
}

extension ProfileTableViewController {
    
    // MARK: User notifications
    
    @objc private func userNameDidEdited(notification: Notification) {
        DispatchQueue.main.async {
            self.updateBarButton()
            if let user = UserManager.shared.current {
                self.delegate?.nameDidChange(name: user.name)
            }
        }
    }
    
    @objc private func userNameDidNotEdited(notification: Notification) {
        DispatchQueue.main.async {
            // say something to user that name did not edited
            print("user name did not edited!")
        }
    }
    
    @objc private func userPhoneDidEdited(notification: Notification) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc private func userPhoneDidNotEdited(notification: Notification) {
        DispatchQueue.main.async {
            // say something to user that name did not edited
            print("user name did not edited!")
        }
    }
    
}
