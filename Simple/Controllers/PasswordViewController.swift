//
//  PasswordViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 01.09.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class PasswordViewController: UIViewController {
    
    // MARK: Properties
    
    private var userService = UserService.shared
    private var doneBtn: UIBarButtonItem!
    
    // MARK: Outlets and actions
    
    @IBOutlet weak var customNavigationItem: UINavigationItem!
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        presentingViewController?.dismiss(animated: true)
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        doneBtn.showCircle()
        view.endEditing(true)
        passwords.forEach { $0.isUserInteractionEnabled = false }
        userService.edit(oldPassword: passwords[0].text!, newPassword: passwords[1].text!)
    }
    
    @IBOutlet var passwords: [UITextField]!
    
    // MARK: Controller methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateBarButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Methods
    
    private func updateBarButton() {
        customNavigationItem.rightBarButtonItem = nil
        let barButton = UIBarButtonItem(title: "Готово", style: .done, target: self, action: #selector(done(_:)))
        barButton.isEnabled = false
        doneBtn = barButton
        customNavigationItem.rightBarButtonItem = barButton
    }

}

extension PasswordViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case passwords[0]: passwords[1].becomeFirstResponder()
        case passwords[1]: passwords[2].becomeFirstResponder()
        case passwords[2]:
            if doneBtn.isEnabled {
                done(doneBtn)
            } else {
                passwords[2].text = ""
            }
        default: break
        }
        return false
    }
    
}

extension PasswordViewController {
    
    // MARK: Register/remove observers
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(textFieldTextDidChange(notification:)), name: .UITextFieldTextDidChange, object: nil)
        
        center.addObserver(self, selector: #selector(passwordDidEdited(notification:)), name: .UserPasswordDidEdited, object: userService)
        center.addObserver(self, selector: #selector(passwordDidNotEdited(notification:)), name: .UserPasswordDidNotEdited, object: userService)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension PasswordViewController {
    
    // MARK: Text field notifications
    
    @objc private func textFieldTextDidChange(notification: Notification) {
        if let text = passwords[1].text {
            doneBtn.isEnabled = !text.isEmpty && text == passwords[2].text! && text != passwords[0].text!
        }
    }
    
}

extension PasswordViewController {
    
    // MARK: User notifications
    
    @objc private func passwordDidEdited(notification: Notification) {
        DispatchQueue.main.async {
            self.presentingViewController?.dismiss(animated: true)
        }
    }
    
    @objc private func passwordDidNotEdited(notification: Notification) {
        DispatchQueue.main.async {
            self.passwords.forEach {
                $0.isUserInteractionEnabled = true
                $0.text = ""
            }
            self.passwords[0].becomeFirstResponder()
            self.updateBarButton()
        }
    }
    
}
