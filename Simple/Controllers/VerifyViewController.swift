//
//  VerifyViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 09.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class VerifyViewController: UIViewController, UITextFieldDelegate, SplitViewControllerDelegateLogout {
    
    // MARK: Properties
    
    private let validator: Validator = Validator()
    private let userFactory: UserFactory = UserFactory()
    private var delay: DispatchTime = DispatchTime.now()
    
    public var service: AuthService?
    public var phone: Phone?
    
    // MARK: Outlets and actions

    @IBAction func back(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true)
    }
    
    @IBOutlet weak var codeTextField: UITextField! {
        didSet {
            codeTextField.delegate = self
        }
    }
    
    @IBAction func tapToEndEditing(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    // MARK: Text field delegate
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, text.count < 6 || string.count == 0 {
            return true
        }
        return false
    }
    
    // MARK: Split view controller delegate logout
    
    public func didLogout() {
        presentingViewController?.dismiss(animated: true)
    }
    
    // MARK: Controller methods
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    private var animator: NavigationAnimator = NavigationAnimator()

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Start segue" {
            segue.destination.transitioningDelegate = animator
            if let vc = segue.destination as? SplitViewController {
                vc.delegateLogout = self
                vc.authService = service
            }
        }
    }
    
    // MARK: - Notifications
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(textFieldDidChange(notification:)), name: .UITextFieldTextDidChange, object: codeTextField)
        center.addObserver(self, selector: #selector(didVerified(notification:)), name: .DidVerified, object: service)
        center.addObserver(self, selector: #selector(didNotVerified(notification:)), name: .DidNotVerified, object: service)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Text field notifications
    
    @objc private func textFieldDidChange(notification: Notification) {
        if let textField = notification.object as? UITextField, let text = textField.text, text.count == 6, validator.isValid(code: text) {
            prepareForStart()
            service?.verify(phone: phone!, with: Code(value: text))
        }
    }
    
    // MARK: Grpc notifications
    
    @objc private func didVerified(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            if let resp = notification.userInfo!["user"] as? auth_User {
                let user = self.userFactory.convert(authUser: resp)
                if UserManager.shared.add(user: user) {
                    self.updateServices()
                    self.performSegue(withIdentifier: "Start segue", sender: nil)
                } else {
                    self.presentingViewController?.dismiss(animated: true)
                }
            }
            self.codeTextField.isEnabled = true
        }
    }
    
    @objc private func didNotVerified(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            self.codeTextField.isEnabled = true
            self.codeTextField.becomeFirstResponder()
        }
    }
    
    // MARK: Helps
    
    private func prepareForStart() {
        codeTextField.isEnabled = false
        view.endEditing(true)
        incDelay(seconds: 0.25)
    }
    
    private func incDelay(seconds: Double) {
        delay = DispatchTime.now() + seconds
    }
    
    private func animateDelay() -> DispatchTime {
        let now = DispatchTime.now()
        if now.rawValue > delay.rawValue {
            return now
        }
        return delay
    }
    
    private func updateServices() {
        let accToken = UserManager.shared.current?.accToken ?? ""
        ChatService.shared.update(accToken: accToken)
        MessageService.shared.update(accToken: accToken)
        UserService.shared.update(accToken: accToken)
    }

}
