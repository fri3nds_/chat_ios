//
//  StartingViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 11.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class StartingViewController: UIViewController {
    
    // MARK: Properties
    
    private var delay: DispatchTime = DispatchTime.now()
    
    // MARK: Outlets
    
    @IBOutlet weak var loader: CircleLoader!
    
    // MARK: Controller methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loader.startAnimating()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        loader.stopAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        incDelay(to: 0.25)
        checkLogin()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    private var animator: NavigationAnimator = NavigationAnimator()

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.transitioningDelegate = animator
        if segue.identifier == "Continue segue" {
            if let vc = segue.destination as? SplitViewController {
                vc.authService = AuthService.shared
            }
        }
    }
    
    // MARK: Helps
    
    private func checkLogin() {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            if UserManager.shared.current != nil {
                self.performSegue(withIdentifier: "Continue segue", sender: nil)
            } else {
                UserManager.shared.deleteAll()
                self.performSegue(withIdentifier: "Login segue", sender: nil)
            }
        }
    }
    
    private func incDelay(to seconds: Double) {
        delay = DispatchTime.now() + seconds
    }
    
    private func animateDelay() -> DispatchTime {
        let now = DispatchTime.now()
        if now.rawValue > delay.rawValue {
            return now
        }
        return delay
    }

}
