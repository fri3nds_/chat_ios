//
//  UsernameViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 30.08.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class UsernameViewController: UIViewController {
    
    // MARK: Delegate
    
    public var delegate: UsernameViewControllerDelegate?
    
    // MARK: Properties
    
    private var userService = UserService.shared
    private var doneBtn: UIBarButtonItem!
    
    // MARK: Outlets and actions
    
    @IBOutlet weak var customNavigationItem: UINavigationItem!
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        doneBtn.showCircle()
        view.endEditing(true)
        username.isUserInteractionEnabled = false
        userService.edit(username: username.text!.trimmingCharacters(in: .whitespacesAndNewlines).lowercased())
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        presentingViewController?.dismiss(animated: true)
    }
    
    @IBOutlet weak var username: UITextField! {
        didSet {
            if let user = UserManager.shared.current {
                username.text = user.username
            }
        }
    }
    
    // MARK: Controller methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateBarButton()
        doneBtn.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Methods
    
    private func updateBarButton() {
        customNavigationItem.rightBarButtonItem = nil
        let barButton = UIBarButtonItem(title: "Готово", style: .done, target: self, action: #selector(done(_:)))
        doneBtn = barButton
        customNavigationItem.rightBarButtonItem = barButton
    }

}

extension UsernameViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty, doneBtn.isEnabled {
            done(doneBtn)
        }
        return false
    }
    
}

extension UsernameViewController {
    
    // MARK: Register/remove observers
    
    public func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(textFieldTextDidChange(notification:)), name: .UITextFieldTextDidChange, object: username)
        
        center.addObserver(self, selector: #selector(usernameDidEdited(notification:)), name: .UserUsernameDidEdited, object: userService)
        center.addObserver(self, selector: #selector(usernameDidNotEdited(notification:)), name: .UserUsernameDidNotEdited, object: userService)
    }
    
    public func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension UsernameViewController {
    
    // MARK: Text field notifications
    
    @objc private func textFieldTextDidChange(notification: Notification) {
        if let textField = notification.object as? UITextField, let user = UserManager.shared.current {
            doneBtn.isEnabled = !textField.text!.isEmpty && textField.text! != user.username
        }
    }
    
}

extension UsernameViewController {
    
    // MARK: User notifications
    
    @objc private func usernameDidEdited(notification: Notification) {
        DispatchQueue.main.async {
            if let user = UserManager.shared.current {
                user.username = self.username.text!.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
                UserManager.shared.save(user: user)
                self.presentingViewController?.dismiss(animated: true, completion: {
                    self.delegate?.usernameDidChange(username: user.username)
                })
            }
        }
    }
    
    @objc private func usernameDidNotEdited(notification: Notification) {
        DispatchQueue.main.async {
            self.username.isUserInteractionEnabled = true
            self.username.becomeFirstResponder()
            self.updateBarButton()
        }
    }
    
}
