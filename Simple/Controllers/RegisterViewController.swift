//
//  RegisterViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 24.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Properties
    
    public var service: AuthService?
    
    // MARK: Outlets and actions
    
    @IBAction func back(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true)
    }
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    
    @IBAction func tapToEndEditing(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    // MARK: Text field delegate
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case username: password.becomeFirstResponder()
        case password: repeatPassword.becomeFirstResponder()
        case repeatPassword: register()
        default: break
        }
        return false
    }
    
    // MARK: Controller methods
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Methods
    
    private func register() {
        view.endEditing(true)
        
        if isCorrectFields() {
            service?.register(
                username: username.text!.trimmingCharacters(in: .whitespacesAndNewlines).lowercased(),
                password: password.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            )
        } else {
            password.text = ""
            repeatPassword.text = ""
            password.becomeFirstResponder()
        }
    }
    
    // MARK: Notifications
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(didRegistered(notification:)), name: .DidRegistered, object: service)
        center.addObserver(self, selector: #selector(didNotRegistered(notification:)), name: .DidNotRegistered, object: service)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Register notifications
    
    @objc private func didRegistered(notification: Notification) {
        DispatchQueue.main.async {
            // do something to say user that register successfully
            self.presentingViewController?.dismiss(animated: true)
        }
    }
    
    @objc private func didNotRegistered(notification: Notification) {
        DispatchQueue.main.async {
            self.password.text = ""
            self.repeatPassword.text = ""
            self.username.becomeFirstResponder()
        }
    }
    
    // MARK: Helps
    
    private func isCorrectFields() -> Bool {
        return !username.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty &&
                !password.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty &&
                !repeatPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty &&
                password.text! == repeatPassword.text!
    }

}
