//
//  TabBarViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 10.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let ContactsAndUsersDidPrepared = Notification.Name("ContactsAndUsersDidPrepared")
    static let ContactsAndUsersDidNotPrepared = Notification.Name("ContactsAndUsersDidNotPrepared")
}

class TabBarViewController: UITabBarController {
    
    // MARK: Properties
    
    private var userService: UserService = UserService.shared
    
    private var userFactory: UserFactory = UserFactory()
    private var phoneFactory: PhoneFactory = PhoneFactory()
    
    // MARK: Controller methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        registerObservers()
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    // MARK: Setup methods
    
    private func setup() {
        selectedIndex = 1
        if !PushCenter.shared.getToken().isEmpty {
            userService.addToken(token: PushCenter.shared.getToken())
        }
        ContactManager.shared.request()
    }

}

extension TabBarViewController {
    
    // MARK: Register/remove observers
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(contactsAccessGranted(notification:)), name: .ContactsAccessGranted, object: ContactManager.shared)
        center.addObserver(self, selector: #selector(contactsAccessDenied(notification:)), name: .ContactsAccessDenied, object: ContactManager.shared)
        center.addObserver(self, selector: #selector(contactsDidFetched(notification:)), name: .ContactsDidFetched, object: ContactManager.shared)
        
        center.addObserver(self, selector: #selector(usersDidGetted(notification:)), name: .UsersDidGetted, object: userService)
        center.addObserver(self, selector: #selector(usersDidNotGetted(notification:)), name: .UsersDidNotGetted, object: userService)
        
        center.addObserver(self, selector: #selector(didLogouted(notification:)), name: .DidLogouted, object: nil)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension TabBarViewController {
    
    // MARK: Contacts notifications
    
    @objc private func contactsAccessGranted(notification: Notification) {
        DispatchQueue.main.async {
            ContactManager.shared.fetch()
        }
    }
    
    @objc private func contactsAccessDenied(notification: Notification) {
        DispatchQueue.main.async {
            // do something
        }
    }
    
    @objc private func contactsDidFetched(notification: Notification) {
        DispatchQueue.main.async {
            self.userService.get(phones: ContactManager.shared.phonesFromContacts())
        }
    }
    
}

extension TabBarViewController {
    
    // MARK: Users notifications
    
    @objc private func usersDidGetted(notification: Notification) {
        DispatchQueue.main.async {
            if let respUsers = notification.userInfo!["users"] as? [user_User] {
                let users = self.userFactory.convert(users: respUsers)
                var contacts = ContactManager.shared.sortedContacts
                UserManager.shared.users = users.map { user in
                    if let index = contacts.index(where: { $0.phones.contains(user.phone.number) }) {
                        user.name = contacts[index].givenName + " " + contacts[index].familyName
                        contacts.remove(at: index)
                    }
                    return user
                }
                ContactManager.shared.sortedContacts = contacts
                
                NotificationCenter.default.post(name: .ContactsAndUsersDidPrepared, object: nil)
            }
        }
    }
    
    @objc private func usersDidNotGetted(notification: Notification) {
        DispatchQueue.main.async {
            print("Holly shit! Users did not getted!")
            
            NotificationCenter.default.post(name: .ContactsAndUsersDidNotPrepared, object: nil)
        }
    }
    
}

extension TabBarViewController {
    
    // MARK: Other notifications
    
    @objc private func didLogouted(notification: Notification) {
        removeObservers()
    }
    
}
