//
//  PhoneViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 09.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: Properties
    
    private let validator: Validator = Validator()
    private let service: AuthService = AuthService.shared
    
    private var delay: DispatchTime = DispatchTime.now()
    private var phone: Phone?
    
    // MARK: Outlet and actions

    @IBOutlet weak var codeBtn: UIButton!
    
    @IBOutlet weak var numberTextField: UITextField! {
        didSet {
            numberTextField.delegate = self
        }
    }
    
    @IBOutlet weak var verifyBtn: UIButton! {
        didSet {
            verifyBtn.setEnable(false)
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        prepareForLogin()
        service.login(with: phone!)
    }
    
    @IBAction func tapToEndEditing(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    // MARK: Controller methods
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation
    
    private let animator: NavigationAnimator = NavigationAnimator()

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.transitioningDelegate = animator
        if segue.identifier == "Verify segue" {
            if let vc = segue.destination as? VerifyViewController {
                vc.service = service
                vc.phone = phone
            }
        } else if segue.identifier == "Without phone" {
            if let vc = segue.destination as? WithoutPhoneViewController {
                vc.service = service
            }
        }
    }
    
    // MARK: Helps
    
    private func prepareForLogin() {
        verifyBtn.showLoading(value: true, image: nil)
        numberTextField.isEnabled = false
        view.endEditing(true)
        incDelay(to: 0.25)
        let code = codeBtn.titleLabel!.text!.after(character: 0)
        phone = Phone(number: code + numberTextField.text!)
    }
    
    private func incDelay(to seconds: Double) {
        delay = DispatchTime.now() + seconds
    }
    
    private func animateDelay() -> DispatchTime {
        let now = DispatchTime.now()
        if now.rawValue > delay.rawValue {
            return now
        }
        return delay
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, text.count < 10 || string.count == 0 {
            return true
        }
        return false
    }
    
}

extension LoginViewController {
    
    // MARK: Notifications
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(textFieldDidChange(notification:)), name: .UITextFieldTextDidChange, object: numberTextField)
        center.addObserver(self, selector: #selector(didLogined(notification:)), name: .DidLogined, object: service)
        center.addObserver(self, selector: #selector(didNotLogined(notification:)), name: .DidNotLogined, object: service)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension LoginViewController {
    
    // MARK: Text field notifications
    
    @objc private func textFieldDidChange(notification: Notification) {
        if let textField = notification.object as? UITextField, let text = textField.text, text.count == 10, validator.isPhone(number: text) {
            verifyBtn.setEnable(true)
        } else {
            verifyBtn.setEnable(false)
        }
    }
    
}

extension LoginViewController {
    
    // MARK: Grpc notifications
    
    @objc private func didLogined(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            self.verifyBtn.showLoading(value: false, image: #imageLiteral(resourceName: "next_arrow"))
            self.numberTextField.isEnabled = true
            self.performSegue(withIdentifier: "Verify segue", sender: nil)
        }
    }
    
    @objc private func didNotLogined(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            self.verifyBtn.showLoading(value: false, image: #imageLiteral(resourceName: "next_arrow"))
            self.numberTextField.isEnabled = true
            
            // Что-то пошло не так
            print("Something wrong")
        }
    }
    
}
