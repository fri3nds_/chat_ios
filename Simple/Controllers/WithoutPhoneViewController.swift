//
//  WithoutPhoneViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 24.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class WithoutPhoneViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Properties
    
    public var service: AuthService?
    
    private var userFactory: UserFactory = UserFactory()
    
    // MARK: Outlets and actions
    
    @IBAction func back(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true)
    }
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBAction func tapToEndEditing(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    // MARK: Text field delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case username: password.becomeFirstResponder()
        case password: login()
        default: break
        }
        return false
    }
    
    // MARK: Controller methods
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    private var animator: NavigationAnimator = NavigationAnimator()

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.transitioningDelegate = animator
        if segue.identifier == "Start without phone" {
            if let vc = segue.destination as? SplitViewController {
                vc.authService = service
            }
        } else if segue.identifier == "Register" {
            if let vc = segue.destination as? RegisterViewController {
                vc.service = service
            }
        }
    }
    
    // MARK: Methods
    
    private func login() {
        view.endEditing(true)
        
        if isCorrectFields() {
            service?.login(
                username: username.text!.trimmingCharacters(in: .whitespacesAndNewlines).lowercased(),
                password: password.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            )
        }
    }

    // MARK: Notifications
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(didLogined(notification:)), name: .DidLogined, object: service)
        center.addObserver(self, selector: #selector(didNotLogined(notification:)), name: .DidNotLogined, object: service)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Login notifications
    
    @objc private func didLogined(notification: Notification) {
        DispatchQueue.main.async {
            if let us = notification.userInfo!["user"] as? auth_User {
                let user = self.userFactory.convert(authUser: us)
                if UserManager.shared.add(user: user) {
                    self.updateServices()
                    self.performSegue(withIdentifier: "Start without phone", sender: nil)
                } else {
                    self.presentingViewController?.dismiss(animated: true)
                }
            }
        }
    }
    
    @objc private func didNotLogined(notification: Notification) {
        DispatchQueue.main.async {
            self.password.becomeFirstResponder()
            self.password.text = ""
        }
    }
    
    // MARK: Helps
    
    private func updateServices() {
        let accToken = UserManager.shared.current?.accToken ?? ""
        ChatService.shared.update(accToken: accToken)
        MessageService.shared.update(accToken: accToken)
        UserService.shared.update(accToken: accToken)
    }
    
    private func isCorrectFields() -> Bool {
        return !username.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty &&
            !password.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
}
