//
//  ChatsViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 16.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit
import AVFoundation

private let reuseIdentifier = "Chat cell"

class ChatsViewController: UIViewController, UISearchResultsUpdating {
    
    // MARK: Properties
    
    private var delay: DispatchTime = DispatchTime.now()
    
    private var chats: [Chat] = []
    private var helpChats: [Chat] = []
    
    private var messages: [String:Message] = [:]
    
    private var userService: UserService = UserService.shared
    private var chatService: ChatService = ChatService.shared
    private var messageService: MessageService = MessageService.shared

    private var userFactory: UserFactory = UserFactory()
    private var chatFactory: ChatFactory = ChatFactory()
    private var messageFactory: MessageFactory = MessageFactory()
    private var phoneFactory: PhoneFactory = PhoneFactory()
    
    private var isFirstLaunch: Bool = true
    
    // MARK: Search controller
    
    private var searchController: UISearchController! {
        didSet {
            searchController.searchResultsUpdater = self
            searchController.hidesNavigationBarDuringPresentation = false
            searchController.searchBar.searchBarStyle = .minimal
            searchController.dimsBackgroundDuringPresentation = false
            definesPresentationContext = true
            searchController.searchBar.sizeToFit()
        }
    }
    
    public func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text, text != "" {
            chats = helpChats.filter { $0.title.lowercased().contains(text.lowercased()) }
        } else {
            chats = helpChats
        }
        chatsTableView.reloadData()
    }
    
    // MARK: Table view
    
    @IBOutlet weak var chatsTableView: UITableView! {
        didSet {
            searchController = UISearchController(searchResultsController: nil)
            chatsTableView.tableHeaderView = searchController.searchBar
        }
    }
    
    // MARK: Controller methods
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { _ in }) { _ in
            self.navigationController?.navigationBar.updateLoaderPosition()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateTabBarBadge()
        
        if navigationItem.isUpdating {
            navigationItem.showUpdating(value: false)
            navigationController?.navigationBar.showUpdating(value: true)
        }
        if let nav = navigationController, !nav.navigationBar.isUpdating {
            tabBarController?.title = "Чаты"
        }
        
        registerObservers()
        if isFirstLaunch {
            tabBarController?.title = ""
            navigationController?.navigationBar.showUpdating(value: true)
            chatService.getChats()
            isFirstLaunch = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        registerRequiredObservers()
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //segue.destination.transitioningDelegate = animator
        if segue.identifier == "Messages from chats" {
            
            if let vc = segue.destination as? MessagesViewController,
                let index = chatsTableView.indexPathForSelectedRow {
                vc.isHiddenScreen = false
                vc.chat = chats[index.row]
            }
        }
    }
    
    // MARK: Setups
    
    private func setup() {
        Updater.shared.addObject(index: 1, method: updateTitle)
    }
    
    // MARK: Chat methods
    
    private func sortChats() {
        helpChats.sort {
            if let f = self.messages[$0.id], let s = self.messages[$1.id] {
                return f.date > s.date
            } else if self.messages[$0.id] != nil {
                return true
            }
            return false
        }
        chats = helpChats
    }
    
    private func updatePlaceOf(chatId: String) {
        let startIndex: Int? = chats.index(where: { $0.id == chatId })
        sortChats()
        let endIndex: Int? = chats.index(where: { $0.id == chatId })
        
        if let startIndex = startIndex, let endIndex = endIndex {
            let start = min(startIndex, endIndex)
            let end = max(startIndex, endIndex)
            
            var indexPaths: [IndexPath] = []
            for index in start ... end {
                indexPaths.append(IndexPath(row: index, section: 0))
            }
            
            chatsTableView.reloadRows(at: indexPaths, with: .automatic)
        }
    }
    
    private func indexForChatWith(_ chats: [Chat], friend: User) -> Int {
        let isMe = friend.curr
        var i = 0
        for chat in chats {
            if chat.type == .dialog {
                if isMe, chat.users[0] == friend.id, chat.users[1] == friend.id {
                    return i
                } else if !isMe, chat.users.contains(friend.id) {
                    return i
                }
            }
            i += 1
        }
        return -1
    }
    
    private func titleFor(chat: Chat) -> String {
        if chat.type == .dialog, let user = UserManager.shared.current {
            let index = chat.users[0] == user.id ? 1 : 0
            if let user = UserManager.shared.get(id: chat.users[index]) {
                chat.title = user.name
                return user.name
            }
        }
        return chat.title
    }
    
    private func idsFromChats() -> [String] {
        var ids: [String] = []
        chats.forEach { ids.append($0.id) }
        return ids
    }
    
    // MARK: Contact methods
    
    private func phonesFrom(contacts: [Contact]) -> [Phone] {
        var phones: [Phone] = []
        contacts.forEach { $0.phones.forEach { phones.append(phoneFactory.convert(phone: $0)) }}
        return phones
    }
    
    // MARK: Help methods
    
    private func updateTitle() {
        if let tabBar = tabBarController, tabBar.selectedIndex == 1 {
            let fade = CATransition()
            fade.duration = 0.25
            fade.type = kCATransitionFade
            navigationController?.navigationBar.layer.add(fade, forKey: "fade")
            
            delayTo(seconds: 0.25) {
                self.tabBarController?.title = "Чаты"
                
                let fade = CATransition()
                fade.duration = 0.25
                fade.type = kCATransitionFade
                self.navigationController?.navigationBar.layer.add(fade, forKey: "fade")
            }
        }
    }
    
    private func incDelay(to seconds: Double) {
        delay = DispatchTime.now() + seconds
    }
    
    private func animateDelay() -> DispatchTime {
        let now = DispatchTime.now()
        if now.rawValue > delay.rawValue {
            return now
        }
        return delay
    }
    
    private func updateTabBarBadge() {
        let badgeCount = UIApplication.shared.applicationIconBadgeNumber
        self.tabBarItem.badgeValue = badgeCount > 0 ? String(badgeCount) : nil
    }
    
    private func playSound() {
        AudioServicesPlayAlertSound(SystemSoundID(1307))
    }
    
}

extension ChatsViewController: UITableViewDelegate {
    
    // MARK: Table view delegate
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let split = parent?.parent?.parent as? SplitViewController,
                let nav = split.viewControllers[1] as? UINavigationController,
                let mvc = nav.viewControllers[0] as? MessagesViewController {
                mvc.clean()
                mvc.isHiddenScreen = false
                mvc.chat = chats[indexPath.row]
                mvc.setup()
            }
        } else {
            performSegue(withIdentifier: "Messages from chats", sender: nil)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension ChatsViewController: UITableViewDataSource {
    
    // MARK: Table view data source
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        let chat = chats[indexPath.row]
        
        if let chatsCell = cell as? ChatsCell {
            chatsCell.chatImage.rounded()
            chatsCell.title.text = titleFor(chat: chat)
            chatsCell.updateBadge(value: chat.badgeCount)
            
            if let lastMessage = messages[chat.id] {
                chatsCell.lastMessageText.text = lastMessage.text
                chatsCell.lastUpdate.text = Date.lastMessage(date: lastMessage.date)
            }
        }
        
        return cell
    }
    
}

extension ChatsViewController {
    
    // MARK: Register/remove observers
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(contactsAndUsersDidPrepared(notification:)), name: .ContactsAndUsersDidPrepared, object: nil)
        center.addObserver(self, selector: #selector(contactsAndUsersDidNotPrepared(notification:)), name: .ContactsAndUsersDidNotPrepared, object: nil)
        
        center.addObserver(self, selector: #selector(userDidGetted(notification: )), name: .UserDidGetted, object: userService)
        
        center.addObserver(self, selector: #selector(refreshDidDone(notification:)), name: .RefreshDidDone, object: nil)
    }
    
    private func removeObservers() {
        let center = NotificationCenter.default
        center.removeObserver(self, name: .ContactsAndUsersDidPrepared, object: nil)
        center.removeObserver(self, name: .ContactsAndUsersDidNotPrepared, object: nil)
        
        center.removeObserver(self, name: .UserDidGetted, object: userService)
        
        center.removeObserver(self, name: .RefreshDidDone, object: nil)
    }
    
    private func registerRequiredObservers() {
        let center = NotificationCenter.default
        
        center.addObserver(self, selector: #selector(pushDialogDidAdded(notification:)), name: .PushDialogDidAdded, object: PushCenter.shared)
        center.addObserver(self, selector: #selector(pushMessageDidAdded(notification:)), name: .PushMessageDidAdded, object: PushCenter.shared)
        center.addObserver(self, selector: #selector(pushLastDidUpdate(notification:)), name: .PushLastDidUpdate, object: PushCenter.shared)
        center.addObserver(self, selector: #selector(pushLastDidUpdateFriend(notification:)), name: .PushLastDidUpdateFriend, object: PushCenter.shared)
        
        center.addObserver(self, selector: #selector(chatsDidGetted(notification:)), name: .ChatsDidGetted, object: chatService)
        center.addObserver(self, selector: #selector(chatsDidNotGetted(notification:)), name: .ChatsDidNotGetted, object: chatService)
        
        center.addObserver(self, selector: #selector(chatDidAdded(notification:)), name: .ChatDidAdded, object: chatService)
        
        center.addObserver(self, selector: #selector(chatLastDidUpdate(notification:)), name: .ChatLastDidUpdate, object: chatService)
        
        center.addObserver(self, selector: #selector(messageDidGetted(notification:)), name: .MessageDidGetted, object: messageService)
        center.addObserver(self, selector: #selector(messageDidAdded(notification:)), name: .MessageDidAdded, object: nil)
        
        center.addObserver(self, selector: #selector(messagesLastDidGetted(notification:)), name: .MessagesLastDidGetted, object: messageService)
        center.addObserver(self, selector: #selector(messagesLastDidNotGetted(notification:)), name: .MessagesLastDidNotGetted, object: messageService)
        
        center.addObserver(self, selector: #selector(didBecomeActive(notification:)), name: .UIApplicationDidBecomeActive, object: nil)
        center.addObserver(self, selector: #selector(didLogouted(notification:)), name: .DidLogouted, object: nil)
        
        center.addObserver(self, selector: #selector(networkIsUnavailable(notification:)), name: .NetworkIsUnavailable, object: nil)
        center.addObserver(self, selector: #selector(networkDidAvailable(notification:)), name: .NetworkDidAvailable, object: nil)
    }
    
    private func removeRequiredObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension ChatsViewController {
    
    // MARK: Contacts and users notifications
    
    @objc private func contactsAndUsersDidPrepared(notification: Notification) {
        DispatchQueue.main.async {
            // reload if needed, this method returns faster than last message method
            if self.chats.count > 0 {
                self.chatsTableView.reloadData()
            }
        }
    }
    
    @objc private func contactsAndUsersDidNotPrepared(notification: Notification) {
        DispatchQueue.main.async {
            // reload if needed, this method returns faster than last message method
            if self.chats.count > 0 {
                self.chatsTableView.reloadData()
            }
        }
    }
    
}

extension ChatsViewController {
    
    // MARK: Chats notifications
    
    @objc private func chatsDidGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            if let chs = notification.userInfo!["chats"] as? [chat_Chat] {
                let chats = self.chatFactory.convert(chats: chs)
                self.chats = chats
                self.helpChats = chats
                self.messageService.getLast(cids: self.idsFromChats())
                self.updateTabBarBadge()
            }
        }
    }
    
    @objc private func chatsDidNotGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            print("Ooops, chats did not getted!")
            self.navigationController?.navigationBar.showUpdating(value: false, completion: {
                if let tabBar = self.tabBarController {
                    Updater.shared.update(index: tabBar.selectedIndex)
                }
            })
            self.updateTabBarBadge()
        }
    }
    
    @objc private func chatDidAdded(notification: Notification) {
        DispatchQueue.main.async {
            if let ch = notification.userInfo!["chat"] as? chat_Chat {
                let chat = self.chatFactory.convert(chat: ch)
                self.chats.insert(chat, at: 0)
                self.helpChats.insert(chat, at: 0)
                self.chatsTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self.messageService.getLast(cids: [chat.id])
            }
        }
    }
    
    @objc private func chatLastDidUpdate(notification: Notification) {
        DispatchQueue.main.async {
            if let chatId = notification.userInfo!["chatId"] as? String, let date = notification.userInfo!["date"] as? Int64 {
                if let index = self.helpChats.index(where: { $0.id == chatId }) {
                    UIApplication.shared.subBadge(value: self.helpChats[index].badgeCount)
                    self.updateTabBarBadge()
                    self.helpChats[index].badgeCount = 0
                    self.helpChats[index].lastUpdate = Int(date)
                }
                
                if let index = self.chats.index(where: { $0.id == chatId }) {
                    self.chatsTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                }
            }
        }
    }
    
}

extension ChatsViewController {
    
    // MARK: Messages notifications
    
    @objc private func messageDidGetted(notification: Notification) {
        DispatchQueue.main.async {
            if let message = notification.userInfo!["message"] as? message_Message {
                let mes = self.messageFactory.convert(message: message)
                self.messages[mes.chatId] = mes
                self.updatePlaceOf(chatId: mes.chatId)
            }
        }
    }
    
    @objc private func messageDidAdded(notification: Notification) {
        DispatchQueue.main.async {
            if let message = notification.userInfo!["message"] as? Message {
                message.date = Int(notification.userInfo!["date"] as! Int64)
                self.messages[message.chatId] = message
                self.updatePlaceOf(chatId: message.chatId)
            }
        }
    }
    
    @objc private func messagesLastDidGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            if let mss = notification.userInfo!["messages"] as? [message_Message] {
                let messages = self.messageFactory.convert(messages: mss)
                self.messages = self.messageFactory.convert(messages: messages)
                self.sortChats()
            }
            self.navigationController?.navigationBar.showUpdating(value: false, completion: {
                if let tabBar = self.tabBarController {
                    Updater.shared.update(index: tabBar.selectedIndex)
                }
            })
            self.chatsTableView.reloadData()
        }
    }
    
    @objc private func messagesLastDidNotGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            print("Ooops, messages did not getted!")
            self.navigationController?.navigationBar.showUpdating(value: false, completion: {
                if let tabBar = self.tabBarController {
                    Updater.shared.update(index: tabBar.selectedIndex)
                }
            })
            self.chatsTableView.reloadData()
        }
    }
    
}

extension ChatsViewController {
    
    // MARK: User notifications
    
    @objc private func userDidGetted(notification: Notification) {
        DispatchQueue.main.async {
            if let user = notification.userInfo!["user"] as? user_User {
                let friend = self.userFactory.convert(uUser: user)
                let helpIndex = self.indexForChatWith(self.helpChats, friend: friend)
                
                if helpIndex != -1 {
                    self.helpChats[helpIndex].title = friend.name
                }
                
                let chatIndex = self.indexForChatWith(self.chats, friend: friend)
                if chatIndex != -1 {
                    self.chatsTableView.reloadRows(at: [IndexPath(row: chatIndex, section: 0)], with: .automatic)
                }
            }
        }
    }
    
}

extension ChatsViewController {
    
    // MARK: Push notifications
    
    @objc private func pushDialogDidAdded(notification: Notification) {
        DispatchQueue.main.async {
            if let di = notification.userInfo!["chat"] as? [String:Any], let user = UserManager.shared.current {
                self.playSound()
                
                let dialog = self.chatFactory.convert(from: di)
                if self.chats.index(where: { $0.id == dialog.id }) == nil {
                    self.chats.insert(dialog, at: 0)
                    self.helpChats.insert(dialog, at: 0)
                    self.chatsTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    
                    let friend = dialog.users[0] == user.id ? dialog.users[1] : dialog.users[0]
                    self.userService.get(id: friend)
                }
            }
        }
    }
    
    @objc private func pushMessageDidAdded(notification: Notification) {
        DispatchQueue.main.async {
            if let mes = notification.userInfo!["message"] as? [String:Any] {
                let message = self.messageFactory.convert(from: mes)
                self.messages[message.chatId] = message
                
                if let index = self.helpChats.index(where: { $0.id == message.chatId }), let user = UserManager.shared.current, user.id != message.userId {
                    self.playSound()
                    let badgeCount = UIApplication.shared.applicationIconBadgeNumber + 1
                    self.helpChats[index].badgeCount += 1
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                    self.updateTabBarBadge()
                }
                self.updatePlaceOf(chatId: message.chatId)
            }
        }
    }
    
    @objc private func pushLastDidUpdate(notification: Notification) {
        DispatchQueue.main.async {
            if let last = notification.userInfo!["last"] as? [String:Any] {
                let chatId = last["chatId"] as! String
                let date = Int(last["date"] as! Int64)
                
                if let index = self.helpChats.index(where: { $0.id == chatId }) {
                    let chat = self.helpChats[index]
                    chat.lastUpdate = date
                    
                    UIApplication.shared.subBadge(value: chat.badgeCount)
                    self.updateTabBarBadge()
                    chat.badgeCount = 0
                }
                
                if let index = self.chats.index(where: { $0.id == chatId }) {
                    self.chatsTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                }
            }
        }
    }
    
    @objc private func pushLastDidUpdateFriend(notification: Notification) {
        DispatchQueue.main.async {
            if let last = notification.userInfo!["last"] as? [String:Any], let chatId = last["chatId"] as? String, let index = self.helpChats.index(where: { $0.id == chatId }) {
                let date = Int(last["date"] as! Int64)
                self.helpChats[index].lastUpdateFriend = date
            }
        }
    }
    
}

extension ChatsViewController {
    
    // MARK: Other notifications
    
    @objc private func refreshDidDone(notification: Notification) {
        DispatchQueue.main.async {
            self.setup()
        }
    }
    
    @objc private func didBecomeActive(notification: Notification) {
        tabBarController?.title = ""
        navigationController?.navigationBar.showUpdating(value: true)
        chatService.getChats()
    }
    
    @objc private func didLogouted(notification: Notification) {
        removeRequiredObservers()
    }
    
}

extension ChatsViewController {
    
    // MARK: Network notifications
    
    @objc private func networkIsUnavailable(notification: Notification) {
        if let nav = navigationController, !nav.navigationBar.isUpdating {
            tabBarController?.title = ""
            navigationController?.navigationBar.showUpdating(value: true)
        }
    }
    
    @objc private func networkDidAvailable(notification: Notification) {
        if navigationItem.isUpdating {
            navigationItem.showUpdating(value: false)
            navigationController?.navigationBar.showUpdating(value: true)
        }
        if let nav = navigationController, nav.navigationBar.isUpdating {
            chatService.getChats()
        }
    }
    
}
