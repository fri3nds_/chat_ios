//
//  SettingsViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 24.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {
    
    // MARK: Properties
    
    public var service: AuthService = AuthService.shared
    public var userService: UserService = UserService.shared
    
    // MARK: Outlets and actions
    
    @IBOutlet weak var avatar: UIImageView! {
        didSet {
            avatar.rounded()
        }
    }
    
    @IBOutlet weak var name: UILabel! {
        didSet {
            if let user = UserManager.shared.current {
                name.text = user.name
            }
        }
    }
    
    @IBOutlet weak var phone: UILabel! {
        didSet {
            if let user = UserManager.shared.current {
                phone.text = user.phone.number
            }
        }
    }
    
    @IBOutlet weak var username: UILabel! {
        didSet {
            if let user = UserManager.shared.current {
                username.text = "@\(user.username)"
            }
        }
    }
    
    // MARK: Controller methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if navigationItem.isUpdating {
            navigationItem.showUpdating(value: false)
            navigationController?.navigationBar.showUpdating(value: true)
        }
        if let nav = navigationController, !nav.navigationBar.isUpdating {
            tabBarController?.title = "Настройки"
        }
        
        registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeObservers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0: performSegue(withIdentifier: "Show profile settings", sender: nil)
        default: break
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Show profile settings" {
            if let vc = segue.destination as? ProfileTableViewController {
                vc.delegate = self
            }
        }
    }
    
    // MARK: Setups
    
    public func setup() {
        Updater.shared.addObject(index: 2, method: updateTitle)
    }
    
    // MARK: Methods
    
    private func updateTitle() {
        if let tabBar = tabBarController, tabBar.selectedIndex == 2 {
            let fade = CATransition()
            fade.duration = 0.25
            fade.type = kCATransitionFade
            navigationController?.navigationBar.layer.add(fade, forKey: "fade")
            
            delayTo(seconds: 0.25) {
                self.tabBarController?.title = "Настройки"
                
                let fade = CATransition()
                fade.duration = 0.25
                fade.type = kCATransitionFade
                self.navigationController?.navigationBar.layer.add(fade, forKey: "fade")
            }
        }
    }

}

extension SettingsViewController: ProfileTableViewControllerDelegate {
    
    public func nameDidChange(name: String) {
        self.name.text = name
    }
    
    public func phoneDidChange(phone: Phone) {
        self.phone.text = phone.number
    }
    
    public func usernameDidChange(username: String) {
        self.username.text = "@\(username)"
    }
    
}

extension SettingsViewController {
    
    // MARK: Register/remove observers
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(didNotLogouted(notification:)), name: .DidNotLogouted, object: service)
    }
    
    private func removeObservers() {
        let center = NotificationCenter.default
        center.removeObserver(self, name: .DidNotLogouted, object: service)
    }
    
}

extension SettingsViewController {
    
    // MARK: User notifications
    
    @objc private func usernameDidEdited(notification: Notification) {
        DispatchQueue.main.async {
            if let user = UserManager.shared.current {
                self.username.text = "@\(user.username)"
            }
        }
    }
    
}

extension SettingsViewController {
    
    // MARK: Logout notifications
    
    @objc private func didNotLogouted(notification: Notification) {
        DispatchQueue.main.async {
            // say something to user that logout is not working
            print("did not logouted!")
        }
    }
    
}
