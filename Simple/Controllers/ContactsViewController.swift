//
//  ContactsViewController.swift
//  Simple
//
//  Created by Nikita Bondar on 10.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController, UISearchResultsUpdating {
    
    // MARK: Properties
    
    private var delay: DispatchTime = DispatchTime.now()
    
    private var users: [User] = []
    private var helpUsers: [User] = []
    
    private var contacts: [Contact] = []
    private var helpContacts: [Contact] = []
    
    private var userFactory: UserFactory = UserFactory()
    private var phoneFactory: PhoneFactory = PhoneFactory()
    
    public var userService: UserService = UserService.shared
    
    private var deferredMethods: [() -> Void] = []
    
    // MARK: Search controller
    
    private var searchController: UISearchController! {
        didSet {
            searchController.searchResultsUpdater = self
            searchController.hidesNavigationBarDuringPresentation = false
            searchController.searchBar.searchBarStyle = .minimal
            searchController.dimsBackgroundDuringPresentation = false
            definesPresentationContext = true
            searchController.searchBar.sizeToFit()
        }
    }
    
    public func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text, text != "" {
            users = helpUsers.filter { $0.name.lowercased().contains(text.lowercased()) }
            contacts = helpContacts.filter { ($0.givenName + " " + $0.familyName).lowercased().contains(text.lowercased()) }
            
            if text[0] == "@" && text.count > 2 {
                userService.search(username: text[1..<text.count])
            }
        } else {
            users = helpUsers
            contacts = helpContacts
        }
        contactsTableView.reloadData()
    }
    
    // MARK: Outlets and actions
    
    @IBOutlet weak var contactsTableView: UITableView! {
        didSet {
            searchController = UISearchController(searchResultsController: nil)
            contactsTableView.tableHeaderView = searchController.searchBar
        }
    }
    
    // MARK: Controller methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if navigationItem.isUpdating {
            navigationItem.showUpdating(value: false)
            navigationController?.navigationBar.showUpdating(value: true)
        }
        if let nav = navigationController, !nav.navigationBar.isUpdating {
            tabBarController?.title = "Контакты"
        }
        
        registerObservers()
        if contacts.count == 0 && users.count == 0 {
            contacts = ContactManager.shared.sortedContacts
            helpContacts = ContactManager.shared.sortedContacts
            
            users = UserManager.shared.users
            helpUsers = UserManager.shared.users
            
            contactsTableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Messages from contacts" {
            if let vc = segue.destination as? MessagesViewController,
                let index = contactsTableView.indexPathForSelectedRow {
                vc.isHiddenScreen = false
                vc.friend = users[index.row]
            }
        }
    }
    
    // MARK: Setups
    
    private func setup() {
        registerRequiredObservers()
        Updater.shared.addObject(index: 0, method: updateTitle)
    }
    
    // MARK: Methods
    
    private func updateTitle() {
        if let tabBar = tabBarController, tabBar.selectedIndex == 0 {
            let fade = CATransition()
            fade.duration = 0.25
            fade.type = kCATransitionFade
            navigationController?.navigationBar.layer.add(fade, forKey: "fade")
            
            delayTo(seconds: 0.25) {
                self.tabBarController?.title = "Контакты"
                
                let fade = CATransition()
                fade.duration = 0.25
                fade.type = kCATransitionFade
                self.navigationController?.navigationBar.layer.add(fade, forKey: "fade")
            }
        }
    }
    
    private func incDelay(to seconds: Double) {
        delay = DispatchTime.now() + seconds
    }
    
    private func animateDelay() -> DispatchTime {
        let now = DispatchTime.now()
        if now.rawValue > delay.rawValue {
            return now
        }
        return delay
    }
    
}

extension ContactsViewController: UITableViewDelegate {
    
    // MARK: Table view delegate
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if UIDevice.current.userInterfaceIdiom == .pad {
                if let split = parent?.parent?.parent as? SplitViewController,
                    let nav = split.viewControllers[1] as? UINavigationController,
                    let mvc = nav.viewControllers[0] as? MessagesViewController {
                    mvc.clean()
                    mvc.isHiddenScreen = false
                    mvc.friend = users[indexPath.row]
                    mvc.setup()
                }
                
                searchController.searchBar.endEditing(true)
            } else {
                performSegue(withIdentifier: "Messages from contacts", sender: nil)
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension ContactsViewController: UITableViewDataSource {
    
    // MARK: Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? users.count : contacts.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "" : "Contacts"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Contact cell", for: indexPath)
        if indexPath.section == 0, let contactsCell = cell as? ContactsCell {
            let user = users[indexPath.row]
            
            contactsCell.avatar.image = nil
            contactsCell.name.text = user.name
            contactsCell.status.text = user.phone.number
        } else if indexPath.section == 1, let contactsCell = cell as? ContactsCell {
            let contact = contacts[indexPath.row]
            
            contactsCell.avatar.image = #imageLiteral(resourceName: "contacts")
            contactsCell.name.text = contact.givenName + " " + contact.familyName
            contactsCell.status.text = contact.phones.count > 0 ? contact.phones[0] : ""
        }
        return cell
    }
    
}

extension ContactsViewController {
    
    // MARK: Register/remove observers
    
    private func registerObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(contactsAndUsersDidPrepared(notification:)), name: .ContactsAndUsersDidPrepared, object: nil)
        center.addObserver(self, selector: #selector(contactsAndUsersDidNotPrepared(notification:)), name: .ContactsAndUsersDidNotPrepared, object: nil)
        
        center.addObserver(self, selector: #selector(userDidGetted(notification:)), name: .UserDidGetted, object: userService)
        center.addObserver(self, selector: #selector(userDidNotGetted(notification:)), name: .UserDidNotGetted, object: userService)
        center.addObserver(self, selector: #selector(usersDidSearched(notification:)), name: .UsersDidSearched, object: userService)
        
        center.addObserver(self, selector: #selector(refreshDidDone(notification:)), name: .RefreshDidDone, object: nil)
    }
    
    private func removeObservers() {
        let center = NotificationCenter.default
        center.removeObserver(self, name: .ContactsAndUsersDidPrepared, object: nil)
        center.removeObserver(self, name: .ContactsAndUsersDidNotPrepared, object: nil)
        
        center.removeObserver(self, name: .UserDidGetted, object: userService)
        center.removeObserver(self, name: .UserDidNotGetted, object: userService)
        center.removeObserver(self, name: .UsersDidSearched, object: userService)
        
        center.removeObserver(self, name: .RefreshDidDone, object: nil)
    }
    
    private func registerRequiredObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(didBecomeActive(notification:)), name: .UIApplicationDidBecomeActive, object: nil)
        center.addObserver(self, selector: #selector(didLogouted(notification:)), name: .DidLogouted, object: nil)
        
        center.addObserver(self, selector: #selector(networkDidAvailable(notification:)), name: .NetworkDidAvailable, object: nil)
    }
    
    private func removeRequiredObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension ContactsViewController {
    
    // MARK: Contacts and users
    
    @objc private func contactsAndUsersDidPrepared(notification: Notification) {
        DispatchQueue.main.async {
            self.users = UserManager.shared.users
            self.helpUsers = UserManager.shared.users
            self.contacts = ContactManager.shared.sortedContacts
            self.helpContacts = ContactManager.shared.sortedContacts
            
            self.contactsTableView.reloadData()
        }
    }
    
    @objc private func contactsAndUsersDidNotPrepared(notification: Notification) {
        DispatchQueue.main.async {
            if ContactManager.shared.sortedContacts.count != 0 {
                self.contacts = ContactManager.shared.sortedContacts
                self.helpContacts = ContactManager.shared.sortedContacts
            }
            self.users = []
            self.helpUsers = []
            
            self.contactsTableView.reloadData()
        }
    }
    
}

extension ContactsViewController {
    
    // MARK: User notifications
    
    @objc private func userDidGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            
        }
    }
    
    @objc private func userDidNotGetted(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: animateDelay()) {
            print("Holly shit! User did not getted!")
        }
    }
    
    @objc private func usersDidSearched(notification: Notification) {
        DispatchQueue.main.async {
            if let uss = notification.userInfo!["users"] as? [user_User] {
                let users = self.userFactory.convert(users: uss)
                if let text = self.searchController.searchBar.text, text[0] == "@" {
                    self.users = users
                    self.contactsTableView.reloadData()
                }
            }
        }
    }
    
}

extension ContactsViewController {
    
    // MARK: Other notifications
    
    @objc private func refreshDidDone(notification: Notification) {
        DispatchQueue.main.async {
            print("refresh did done! cont!")
        }
    }
    
    @objc private func didBecomeActive(notification: Notification) {
        ContactManager.shared.fetch()
    }
    
    @objc private func didLogouted(notification: Notification) {
        removeRequiredObservers()
    }
    
}

extension ContactsViewController {
    
    // MARK: Network notifications
    
    @objc private func networkDidAvailable(notification: Notification) {
        if let nav = navigationController, nav.navigationBar.isUpdating {
            ContactManager.shared.fetch()
        }
    }
    
}
