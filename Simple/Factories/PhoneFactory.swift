//
//  PhoneFactory.swift
//  Simple
//
//  Created by Nikita Bondar on 08.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

class PhoneFactory {
    
    // MARK: Convert part
    
    public func convert(phone: auth_Phone) -> Phone {
        return Phone(number: phone.number)
    }
    
    public func convert(phone: user_Phone) -> Phone {
        return Phone(number: phone.number)
    }
    
    public func convert(phone: String) -> Phone {
        return Phone(number: phone)
    }
    
    public func convert(phones: [String]) -> [Phone] {
        var phs: [Phone] = []
        phones.forEach { phs.append(convert(phone: $0)) }
        return phs
    }
    
    // MARK: Reverse convert part
    
    public func reverse(phone: Phone) -> auth_Phone {
        var ph = auth_Phone()
        ph.number = phone.number
        return ph
    }
    
    public func reverse(phone: Phone) -> user_Phone {
        var ph = user_Phone()
        ph.number = phone.number
        return ph
    }
    
    public func reverse(phones: [Phone]) -> [user_Phone] {
        var phs: [user_Phone] = []
        phones.forEach { phs.append(reverse(phone: $0)) }
        return phs
    }
    
}
