//
//  ChatFactory.swift
//  Simple
//
//  Created by Nikita Bondar on 12.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

class ChatFactory {
    
    // MARK: Convert part
    
    public func convert(chat: chat_Chat) -> Chat {
        var users: [String] = []
        chat.users.forEach { users.append($0.value) }
        var admins: [String] = []
        chat.admins.forEach { admins.append($0.value) }
        return Chat(
            id:
            chat.id,
            title: chat.title,
            type: Int(chat.type),
            created: Int(chat.created),
            users: users,
            admins: admins,
            lastUpdate: Int(chat.lastUpdate),
            lastUpdateFriend: Int(chat.lastUpdateFriend),
            badgeCount: Int(chat.badgeCount)
        )
    }
    
    public func convert(chats: [chat_Chat]) -> [Chat] {
        var chs: [Chat] = []
        chats.forEach { chs.append(convert(chat: $0)) }
        return chs
    }
    
    public func convert(from push: [String:Any]) -> Chat {
        return Chat(
            id: push["id"] as! String,
            title: push["title"] as! String,
            type: push["type"] as! Int,
            created: push["created"] as! Int,
            users: push["users"] as! [String],
            admins: push["admins"] as! [String],
            lastUpdate: Int(push["lastUpdate"] as! Int64),
            lastUpdateFriend: Int(push["lastUpdateFriend"] as! Int64),
            badgeCount: Int(push["badgeCount"] as! Int32)
        )
    }
    
    // MARK: Reverse convert part
    
}
