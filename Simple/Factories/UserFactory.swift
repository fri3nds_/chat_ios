//
//  UserFactory.swift
//  Simple
//
//  Created by Nikita Bondar on 08.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

class UserFactory {
    
    private let phoneFactory = PhoneFactory()
    private let statusFactory = StatusFactory()
    
    // MARK: Convert part
    
    public func convert(authUser: auth_User) -> User {
        let user: User!
        
        if let us = UserManager.shared.current {
            user = us
        } else {
            user = User(context: UserManager.shared.context)
        }
        
        user.id = authUser.id
        user.name = authUser.name
        user.username = authUser.username
        user.phone = phoneFactory.convert(phone: authUser.phone)
        user.status = statusFactory.convert(status: authUser.status)
        user.accToken = authUser.accToken
        user.refToken = authUser.refToken
        user.curr = true
        
        return user
    }
    
    public func convert(uUser: user_User) -> User {
        let user: User!
        
        if let us = UserManager.shared.get(id: uUser.id) {
            user = us
            user.name = !user.curr && !user.name.isEmpty ? user.name : uUser.name
        } else {
            user = User(context: UserManager.shared.context)
            user.name = uUser.name
            user.curr = false
        }
        
        user.id = uUser.id
        user.username = uUser.username
        user.phone = phoneFactory.convert(phone: uUser.phone)
        user.status = statusFactory.convert(status: uUser.status)
        
        user.accToken = user.curr ? user.accToken : ""
        user.refToken = user.curr ? user.refToken : ""
        
        return user
    }
    
    public func convert(users: [user_User]) -> [User] {
        var res: [User] = []
        users.forEach { res.append(convert(uUser: $0)) }
        return res
    }
    
}
