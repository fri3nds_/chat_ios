//
//  StatusFactory.swift
//  Simple
//
//  Created by Nikita Bondar on 08.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

class StatusFactory {
    
    // MARK: Convert part
    
    public func convert(status: auth_Status) -> Status {
        return Status(value: Int(status.value), date: Int(status.date))
    }
    
    public func convert(status: user_Status) -> Status {
        return Status(value: Int(status.value), date: Int(status.date))
    }
    
    // MARK: Reverse convert part
    
    public func reverse(status: Status) -> user_Status {
        var st = user_Status()
        st.value = Int32(status.value.rawValue)
        st.date = Int64(status.date)
        return st
    }
    
}
