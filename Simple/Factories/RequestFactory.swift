//
//  RequestFactory.swift
//  Simple
//
//  Created by Nikita Bondar on 08.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

class RequestFactory {
    
    // MARK: Factories
    
    private var phoneFactory: PhoneFactory = PhoneFactory()
    private var statusFactory: StatusFactory = StatusFactory()
    private var messageFactory: MessageFactory = MessageFactory()
    
    // MARK: Auth part
    
    public func authLoginReq(with ph: Phone) -> auth_Phone {
        return phoneFactory.reverse(phone: ph)
    }
    
    public func authVerifyReq(with phone: Phone, and code: Code) -> auth_VerifyReq {
        var req = auth_VerifyReq()
        req.phone = phoneFactory.reverse(phone: phone)
        req.code = auth_Code()
        req.code.value = code.value
        return req
    }
    
    public func authLoginWithoutPhoneReq(username: String, password: String) -> auth_LoginWithoutPhoneReq {
        var req = auth_LoginWithoutPhoneReq()
        req.username = username
        req.password = password
        return req
    }
    
    public func authRegisterReq(username: String, password: String) -> auth_RegisterReq {
        var req = auth_RegisterReq()
        req.username = username
        req.password = password
        return req
    }
    
    public func authLogoutReq(user: User, devToken: String) -> auth_LogoutReq {
        var req = auth_LogoutReq()
        req.accToken = user.accToken
        req.devToken = devToken
        return req
    }
    
    public func authRefReq(user: User) -> auth_RefReq {
        var req = auth_RefReq()
        req.refToken = user.refToken
        return req
    }
    
    // MARK: Chat part
    
    public func chatGetChatReq(accToken: String, id: String) -> chat_GetChatReq {
        var req = chat_GetChatReq()
        req.accToken = accToken
        var cid = chat_Id()
        cid.value = id
        req.id = cid
        return req
    }
    
    public func chatGetChatWithReq(accToken: String, uid: String) -> chat_GetChatWithReq {
        var req = chat_GetChatWithReq()
        req.accToken = accToken
        var id = chat_UserId()
        id.value = uid
        req.id = id
        return req
    }
    
    public func chatGetChatsReq(accToken: String) -> chat_GetChatsReq {
        var req = chat_GetChatsReq()
        req.accToken = accToken
        return req
    }
    
    public func chatAddDialogReq(accToken: String, uid: String, token: String) -> chat_AddDialogReq {
        var req = chat_AddDialogReq()
        req.accToken = accToken
        var id = chat_UserId()
        id.value = uid
        req.id = id
        req.devToken = token
        return req
    }
    
    public func chatAddGroupReq(accToken: String, title: String, uids: [String], token: String) -> chat_AddGroupReq {
        var req = chat_AddGroupReq()
        req.accToken = accToken
        req.title = title
        var ids: [chat_UserId] = []
        uids.forEach { var id = chat_UserId(); id.value = $0; ids.append(id) }
        req.users = ids
        req.devToken = token
        return req
    }
    
    public func chatAddChannelReq(accToken: String, title: String, token: String) -> chat_AddChannelReq {
        var req = chat_AddChannelReq()
        req.accToken = accToken
        req.title = title
        req.devToken = token
        return req
    }
    
    public func chatEditChatReq(accToken: String, id: String, title: String, token: String) -> chat_EditChatReq {
        var req = chat_EditChatReq()
        req.accToken = accToken
        req.title = title
        var cid = chat_Id()
        cid.value = id
        req.id = cid
        req.devToken = token
        return req
    }
    
    public func chatAddUserReq(accToken: String, id: String, uid: String, token: String) -> chat_AddUserReq {
        var req = chat_AddUserReq()
        req.accToken = accToken
        var cid = chat_Id()
        cid.value = id
        req.id = cid
        var id = chat_UserId()
        id.value = uid
        req.userID = id
        req.devToken = token
        return req
    }
    
    public func chatDelUserReq(accToken: String, id: String, uid: String, token: String) -> chat_DelUserReq {
        var req = chat_DelUserReq()
        req.accToken = accToken
        var cid = chat_Id()
        cid.value = id
        req.id = cid
        var id = chat_UserId()
        id.value = uid
        req.userID = id
        req.devToken = token
        return req
    }
    
    public func chatUpdateLast(accToken: String, chatId: String, token: String) -> chat_UpdateLastReq {
        var req = chat_UpdateLastReq()
        req.accToken = accToken
        req.devToken = token
        var id = chat_Id()
        id.value = chatId
        req.chatID = id
        return req
    }
    
    // MARK: Message part
    
    public func mesGetMessageReq(accToken: String, id: String) -> message_GetMessageReq {
        var req = message_GetMessageReq()
        req.accToken = accToken
        var mid = message_Id()
        mid.value = id
        return req
    }
    
    public func mesGetMessagesReq(accToken: String, cid: String, page: Int) -> message_GetMessagesReq {
        var req = message_GetMessagesReq()
        req.accToken = accToken
        var c_id = message_ChatId()
        c_id.value = cid
        req.id = c_id
        req.page = Int32(page)
        return req
    }

    public func mesGetLastMessagesReq(accToken: String, cids: [String]) -> message_GetLastMessagesReq {
        var req = message_GetLastMessagesReq()
        req.accToken = accToken
        var ids: [message_ChatId] = []
        cids.forEach { var id = message_ChatId(); id.value = $0; ids.append(id) }
        req.ids = ids
        return req
    }
    
    public func mesGetUpdatesReq(accToken: String, chatId: String, date: Int) -> message_GetUpdatesReq {
        var req = message_GetUpdatesReq()
        req.accToken = accToken
        var id = message_ChatId()
        id.value = chatId
        req.id = id
        req.date = Int64(date)
        return req
    }
    
    public func mesAddMessageReq(accToken: String, cid: String, text: String, mids: [String], token: String) -> message_AddMessageReq {
        var req = message_AddMessageReq()
        req.accToken = accToken
        var c_id = message_ChatId()
        c_id.value = cid
        req.chatID = c_id
        req.text = text
        var ids: [message_Id] = []
        mids.forEach { var id = message_Id(); id.value = $0; ids.append(id) }
        req.messages = ids
        req.devToken = token
        return req
    }
    
    public func mesEditMessageReq(accToken: String, message: Message, token: String) -> message_EditMessageReq {
        var req = message_EditMessageReq()
        req.accToken = accToken
        req.message = messageFactory.reverse(message: message)
        req.devToken = token
        return req
    }
    
    public func mesDelMessageReq(accToken: String, id: String, token: String) -> message_DelMessageReq {
        var req = message_DelMessageReq()
        req.accToken = accToken
        var mid = message_Id()
        mid.value = id
        req.id = mid
        req.devToken = token
        return req
    }
    
    // MARK: User part
    
    public func userGetReq(accToken: String, id: String) -> user_GetReq {
        var req = user_GetReq()
        req.accToken = accToken
        var uid = user_Id()
        uid.value = id
        req.id = uid
        return req
    }
    
    public func userGetByUsernameReq(accToken: String, username: String) -> user_GetByUsernameReq {
        var req = user_GetByUsernameReq()
        req.accToken = accToken
        req.username = username
        return req
    }
    
    public func userGetByPhoneReq(accToken: String, phone: Phone) -> user_GetByPhoneReq {
        var req = user_GetByPhoneReq()
        req.accToken = accToken
        req.phone = phoneFactory.reverse(phone: phone)
        return req
    }
    
    public func userGetByPhonesReq(accToken: String, phones: [Phone]) -> user_GetByPhonesReq {
        var req = user_GetByPhonesReq()
        req.accToken = accToken
        req.phones = phoneFactory.reverse(phones: phones)
        return req
    }
    
    public func userSearchByUsername(accToken: String, username: String) -> user_SearchByUsernameReq {
        var req = user_SearchByUsernameReq()
        req.accToken = accToken
        req.username = username
        return req
    }
    
    public func userEditNameReq(accToken: String, name: String, token: String) -> user_EditNameReq {
        var req = user_EditNameReq()
        req.accToken = accToken
        req.name = name
        req.devToken = token
        return req
    }
    
    public func userEditUsernameReq(accToken: String, username: String, token: String) -> user_EditUsernameReq {
        var req = user_EditUsernameReq()
        req.accToken = accToken
        req.username = username
        req.devToken = token
        return req
    }
    
    public func userEditPhoneReq(accToken: String, phone: Phone, token: String) -> user_EditPhoneReq {
        var req = user_EditPhoneReq()
        req.accToken = accToken
        req.phone = phoneFactory.reverse(phone: phone)
        req.devToken = token
        return req
    }
    
    public func userEditPasswordReq(accToken: String, oldPassword: String, newPassword: String) -> user_EditPasswordReq {
        var req = user_EditPasswordReq()
        req.accToken = accToken
        req.oldPassword = oldPassword
        req.newPassword = newPassword
        return req
    }
    
    public func userSetStatusReq(accToken: String, status: Status, token: String) -> user_SetStatusReq {
        var req = user_SetStatusReq()
        req.accToken = accToken
        req.token = token
        req.status = statusFactory.reverse(status: status)
        return req
    }
    
    public func userAddTokenReq(accToken: String, token: String) -> user_AddTokenReq {
        var req = user_AddTokenReq()
        req.accToken = accToken
        req.token = token
        return req
    }
    
    public func userDelTokenReq(accToken: String, token: String) -> user_DelTokenReq {
        var req = user_DelTokenReq()
        req.accToken = accToken
        req.token = token
        return req
    }
    
}
