//
//  MessageFactory.swift
//  Simple
//
//  Created by Nikita Bondar on 12.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

class MessageFactory {
    
    // MARK: New part
    
    public func new(text: String, userId: String, chatId: String, messages: [Message]) -> Message {
        return Message(
            id: String.random(n: 32),
            text: text,
            userId: userId,
            chatId: chatId,
            date: Date().millisecondsSince1970,
            messages: messages
        )
    }
    
    // MARK: Convert part
    
    public func convert(message: message_Message) -> Message {
        var messages: [Message] = []
        message.messages.forEach { messages.append(convert(message: $0)) }
        
        return Message(
            id: message.id,
            text: message.text,
            userId: message.userID,
            chatId: message.chatID,
            date: Int(message.date),
            messages: messages
        )
    }
    
    public func convert(messages: [message_Message]) -> [Message] {
        var res: [Message] = []
        messages.forEach { res.append(convert(message: $0)) }
        return res
    }
    
    public func convert(messages: [Message]) -> [String:Message] {
        var dict: [String:Message] = [:]
        messages.forEach { dict[$0.chatId] = $0 }
        return dict
    }
    
    public func convert(from push: [String:Any]) -> Message {
        return Message(
            id: push["id"] as! String,
            text: push["text"] as! String,
            userId: push["userId"] as! String,
            chatId: push["chatId"] as! String,
            date: push["date"] as! Int,
            messages: []
        )
    }
    
    // MARK: Reverse convert part
    
    public func reverse(message: Message) -> message_ShortMessage {
        var short = message_ShortMessage()
        short.id = message.id
        short.text = message.text
        short.userID = message.userId
        short.chatID = message.chatId
        short.date = Int64(message.date)
        short.messages = []
        message.messages.forEach { var id = message_Id(); id.value = $0.id; short.messages.append(id) }
        return short
    }
    
}
