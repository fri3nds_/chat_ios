//
//  UICollectionView.swift
//  Simple
//
//  Created by Nikita Bondar on 15.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    public func updateScrollInsets() {
        if bounds.width != scrollIndicatorInsets.right {
            scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: bounds.width - 6)
        }
    }
    
    public func reloadVisibleCells(in section: Int) {
        if numberOfItems(inSection: section) > 0 {
            performBatchUpdates({
                var indexes: [IndexPath] = []
                self.visibleCells.forEach { cell in
                    if let index = indexPath(for: cell) {
                        indexes.append(index)
                    }
                }
                self.reloadItems(at: indexes)
            })
        }
    }
    
}
