//
//  Date.swift
//  Simple
//
//  Created by Nikita Bondar on 13.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

extension Date {
    
    public var millisecondsSince1970: Int {
        let millisecond = 1000000000.0
        return Int((self.timeIntervalSince1970 * millisecond).rounded())
    }
    
    public static func hourAndMinute(date: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(date))
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        return "\(hour):\(minute < 10 ? "0" + String(minute) : String(minute))"
    }
    
    public static func dayAndMonth(date: Int) -> String {
        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: TimeInterval(date))
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        return "\(day < 10 ? "0" + String(day) : String(day)).\(month < 10 ? "0" + String(month) : String(month))"
    }
    
    public func isToday(date: Int) -> Bool {
        let date = Date(timeIntervalSince1970: TimeInterval(date))
        let calendar = Calendar.current
        let dateDay = calendar.component(.day, from: date)
        let dateMonth = calendar.component(.month, from: date)
        let dateYear = calendar.component(.year, from: date)
        let currDay = calendar.component(.day, from: self)
        let currMonth = calendar.component(.month, from: self)
        let currYear = calendar.component(.year, from: self)
        return dateDay == currDay && dateMonth == currMonth && dateYear == currYear
    }
    
    public static func lastMessage(date: Int) -> String {
        let millisecond = 1000000000
        if Date().isToday(date: date / millisecond) {
            return Date.hourAndMinute(date: date / millisecond)
        }
        return Date.dayAndMonth(date: date / millisecond)
    }
    
    public static func messageTime(date: Int) -> String {
        let millisecond = 1000000000
        return Date.hourAndMinute(date: date / millisecond)
    }
    
}
