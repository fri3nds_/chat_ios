//
//  NavigationBar.swift
//  Simple
//
//  Created by Nikita Bondar on 26.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    public var loaderTag: Int {
        return 10
    }
    
    public var isUpdating: Bool {
        return viewWithTag(loaderTag) != nil
    }
    
    public func showUpdating(value: Bool, completion: (() -> Void)? = nil) {
        let tag = loaderTag
        
        if value && viewWithTag(tag) == nil {
            let view = UIView(frame: CGRect(x: center.x - 71, y: 12, width: 142, height: 20))
            view.tag = tag
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 114, height: 20))
            label.text = "Обновление..."
            
            let loader = CircleLoader(frame: CGRect(x: 122, y: 0, width: 20, height: 20))
            loader.startAnimating()
            
            view.addSubview(label)
            view.addSubview(loader)
            
            addSubview(view)
        } else if !value {
            if let view = viewWithTag(tag) {
                view.removeFromSuperview()
            }
        }
        
        completion?()
    }
    
    public func updateLoaderPosition() {
        if let loader = viewWithTag(loaderTag) {
            UIView.animate(withDuration: 0.2) {
                loader.frame = CGRect(x: self.center.x - 71, y: 12, width: 142, height: 20)
            }
        }
    }
    
}
