//
//  UIBarButtonItem.swift
//  Simple
//
//  Created by Nikita Bondar on 26.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    public func showLoading() {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
        title = ""
        customView = indicator
    }
    
    public func showCircle() {
        let indicator = CircleLoader(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        indicator.startAnimating()
        title = ""
        customView = indicator
    }
    
}
