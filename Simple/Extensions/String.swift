//
//  String.swift
//  Simple
//
//  Created by Nikita Bondar on 09.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension String {
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    public static func random(n: Int) -> String {
        let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        var str = ""
        for _ in 0 ..< n {
            str += letters[Int.random(n: letters.count)]
        }
        return str
    }
    
    public func after(character index: Int) -> String {
        let array = Array(self)
        var str = ""
        if array.count > 0 {
            for i in (index + 1)..<self.count {
                str += String(array[i])
            }
        }
        return str
    }
    
    subscript (i: Int) -> Character {
        return self[index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i])
    }
    
    subscript (range: Range<Int>) -> String {
        let start = index(self.startIndex, offsetBy: range.lowerBound)
        let end = index(self.startIndex, offsetBy: range.upperBound)
        return String(self[Range(start..<end)])
    }
    
    public func changeFirstEight(to char: Character) -> String {
        if let ch = self.first, ch == Character("8") {
            var newStr = Array(self)
            newStr[0] = char
            return String(newStr)
        }
        return self
    }
    
}
