//
//  UILabel.swift
//  Simple
//
//  Created by Nikita Bondar on 09.09.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UILabel {
    
    public func estimatedSize(forWidth width: CGFloat) -> CGSize {
        let textSize = font!.pointSize
        let estimated = NSString(string: text!).boundingRect(
            with: CGSize(width: width, height: 100.0),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.systemFont(ofSize: textSize)],
            context: nil
        )
        return estimated.size
    }
    
}
