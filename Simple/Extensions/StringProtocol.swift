//
//  StringProtocol.swift
//  Simple
//
//  Created by Nikita Bondar on 11.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

extension StringProtocol {
    
    var ascii: [UInt32] {
        return unicodeScalars.filter{ $0.isASCII }.map{ $0.value }
    }
    
    public func onlyNumbers() -> String {
        let ascii = self.ascii
        var result = ""
        ascii.filter { $0 >= 48 && $0 <= 57 }.forEach { result.append(Character(UnicodeScalar($0)!)) }
        return result
    }
    
}
