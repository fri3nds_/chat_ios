//
//  ImageView.swift
//  Simple
//
//  Created by Nikita Bondar on 11.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UIImageView {
    
    public func rounded() {
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
}
