//
//  UINavigationItem.swift
//  Simple
//
//  Created by Nikita Bondar on 09.09.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UINavigationItem {
    
    public var titleTag: Int {
        return 25
    }
    
    public func show(title: String?, with status: Status? = nil) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 20))
        label.text = title
        label.frame.size = label.estimatedSize(forWidth: 1000)
        
        let statusLabel = UILabel(frame: CGRect(x: label.frame.width + 5.0, y: 8.0, width: 8.0, height: 8.0))
        statusLabel.layer.cornerRadius = 4.0
        statusLabel.layer.borderWidth = 1.0
        statusLabel.layer.borderColor = UIColor.darkGray.cgColor
        statusLabel.layer.backgroundColor = status != nil && status!.value == .online
            ? UIColor.darkGray.cgColor : UIColor.white.cgColor
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: label.frame.width + 10.0 + 8.0, height: 20.0))
        view.tag = titleTag
        
        view.addSubview(label)
        view.addSubview(statusLabel)
        
        titleView = view
    }
    
    public func hideTitle() {
        titleView = nil
    }
    
    public var updatingTag: Int {
        return 30
    }
    
    public var isUpdating: Bool {
        return titleView != nil && titleView!.tag == updatingTag
    }
    
    public func showUpdating(value: Bool, completion: (() -> Void)? = nil) {
        if value && titleView?.viewWithTag(updatingTag) == nil {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 142, height: 20))
            view.tag = updatingTag
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 114, height: 20))
            label.text = "Обновление..."
            
            let loader = CircleLoader(frame: CGRect(x: 122, y: 0, width: 20, height: 20))
            loader.startAnimating()
            
            view.addSubview(label)
            view.addSubview(loader)
            
            titleView = view
        } else if !value {
            titleView = nil
        }
        completion?()
    }
    
}

