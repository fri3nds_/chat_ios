//
//  UIt.swift
//  Simple
//
//  Created by Nikita Bondar on 12.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UITextView {
    
    public var isEmpty: Bool {
        return text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || textColor == #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    public func placeholder(text: String, color: UIColor) {
        self.text = text
        textColor = color
    }
    
    public func estimatedSize(forText text: String) -> CGSize {
        let textSize = font!.pointSize
        let estimated = NSString(string: text).boundingRect(
            with: CGSize(width: bounds.size.width - 10.0, height: 100.0),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.systemFont(ofSize: textSize)],
            context: nil
        )
        return estimated.size
    }
    
    public func alignTextVertically() {
        var topCorrect = (bounds.height - contentSize.height * zoomScale) / 2
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect
        contentInset.top = topCorrect
    }
    
}
