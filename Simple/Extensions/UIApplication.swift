//
//  UIApplication.swift
//  Simple
//
//  Created by Nikita Bondar on 05.09.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UIApplication {
    
    public func subBadge(value: Int) {
        if applicationIconBadgeNumber - value > 0 {
            applicationIconBadgeNumber -= value
        } else {
            applicationIconBadgeNumber = 0
        }
    }
    
}
