//
//  Button.swift
//  Simple
//
//  Created by Nikita Bondar on 09.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

extension UIButton {
    
    public var isLoading: Bool {
        if self.subviews.last as? CircleLoader != nil {
            return true
        }
        return false
    }
    
    public func showLoading(value: Bool, title: String?) {
        showLoading(value: value, title: title, image: nil)
    }
    
    public func showLoading(value: Bool, image: UIImage?) {
        showLoading(value: value, title: nil, image: image)
    }
    
    private func showLoading(value: Bool, title: String?, image: UIImage?) {
        let tag = 10
        if value {
            isEnabled = false
            alpha = 0.5
            
            let indicator = CircleLoader(frame: CGRect(x: bounds.width / 2, y: bounds.height / 2, width: 20.0, height: 20.0))
            indicator.tag = tag
            
            if let text = title {
                setTitle(text, for: .normal)
            } else {
                setImage(image, for: .normal)
            }
            addSubview(indicator)
            indicator.startAnimating()
            
        } else {
            isEnabled = true
            alpha = 1.0
            
            if let indicator = viewWithTag(tag) as? CircleLoader {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
            
            if let text = title {
                setTitle(text, for: .normal)
            } else {
                setImage(image, for: .normal)
            }
        }
    }
    
    public func setEnable(_ value: Bool) -> Void {
        if value {
            if !self.isEnabled {
                UIView.animate(withDuration: 0.25) {
                    self.isEnabled = true
                    self.alpha = 1.0
                }
            }
        } else {
            if self.isEnabled {
                UIView.animate(withDuration: 0.25) {
                    self.isEnabled = false
                    self.alpha = 0.2
                }
            }
        }
    }
    
    public func rounded() {
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
}
