//
//  Int.swift
//  Simple
//
//  Created by Nikita Bondar on 13.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

extension Int {
    
    public static func random(n: Int) -> Int {
        return Int(arc4random_uniform(UInt32(n)))
    }
    
}
