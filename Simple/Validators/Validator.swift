//
//  Validator.swift
//  Simple
//
//  Created by Nikita Bondar on 09.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

class Validator {
    
    public func isPhone(number: String) -> Bool {
        let pat = "[0-9]{10}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", pat)
        return predicate.evaluate(with: number)
    }
    
    public func isValid(code: String) -> Bool {
        let pat = "[0-9]{6}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", pat)
        return predicate.evaluate(with: code)
    }
    
}
