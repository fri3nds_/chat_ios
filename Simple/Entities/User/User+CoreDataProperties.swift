//
//  User+CoreDataProperties.swift
//  
//
//  Created by Nikita Bondar on 10.07.2018.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var id: String
    @NSManaged public var name: String
    @NSManaged public var username: String
    @NSManaged public var ph: String
    @NSManaged public var st: Data
    @NSManaged public var accToken: String
    @NSManaged public var refToken: String
    @NSManaged public var curr: Bool

}
