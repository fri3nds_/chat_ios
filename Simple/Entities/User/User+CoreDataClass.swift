//
//  User+CoreDataClass.swift
//  
//
//  Created by Nikita Bondar on 07.07.2018.
//
//

import Foundation
import CoreData

public class User: NSManagedObject {
    
    // MARK: Phone
    
    private var helpPhone: Phone?
    
    public var phone: Phone {
        get {
            if let p = helpPhone {
                return p
            }
            helpPhone = Phone(number: ph)
            return helpPhone!
        }
        set {
            helpPhone = newValue
            ph = newValue.number
        }
    }
    
    // MARK: Status
    
    private var helpStatus: Status?
    
    public var status: Status {
        get {
            if let help = helpStatus {
                return help
            }
            helpStatus = Status(jsonData: st)
            return helpStatus!
        }
        set {
            helpStatus = newValue
            st = newValue.json()
        }
    }
    
    // MARK: Override
    
    override public var description: String {
        return "\n=========== User ============\n" +
            "id: \(id)\n" +
            "name: \(name)\n" +
            "username: \(username)\n" +
            "phone: \(phone.description)\n" +
            "status: \(status.description)\n" +
            "accToken: \(accToken)\n" +
            "refToken: \(refToken)\n" +
            "curr: \(curr)\n" +
            "=============================\n"
    }
    
}
