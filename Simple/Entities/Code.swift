//
//  Code.swift
//  Simple
//
//  Created by Nikita Bondar on 07.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

public struct Code {
    
    var value: String
    
    init(value: String) {
        self.value = value
    }
    
}
