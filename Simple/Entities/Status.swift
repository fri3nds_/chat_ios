//
//  Status.swift
//  Simple
//
//  Created by Nikita Bondar on 08.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

public class Status {
    
    public enum StatusValue: Int {
        case offline = 0
        case online = 1
    }
    
    public var value: StatusValue
    public var date: Int
    
    public var description: String {
        return "\n=========== Status ===========\n" +
        "value: \(value.rawValue)\n" +
        "date: \(date)\n" +
        "==============================\n"
    }
    
    public func json() -> Data {
        let dict = ["value": value.rawValue, "date": date]
        return try! JSONSerialization.data(withJSONObject: dict, options: [])
    }
    
    public init(value: Int, date: Int) {
        self.value = StatusValue(rawValue: value)!
        self.date = date
    }
    
    public init(value: StatusValue, date: Int) {
        self.value = value
        self.date = date
    }
    
    public init(jsonData: Data) {
        if let json = (try? JSONSerialization.jsonObject(with: jsonData, options: [])) as? [String:Any] {
            self.value = StatusValue(rawValue: json["value"] as! Int)!
            self.date = json["date"] as! Int
        } else {
            self.value = StatusValue(rawValue: 0)!
            self.date = 0
        }
    }
    
}
