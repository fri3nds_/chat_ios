//
//  Contact.swift
//  Simple
//
//  Created by Nikita Bondar on 10.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation
import Contacts

public class Contact {
    
    // MARK: Properties
    
    public var givenName: String
    public var familyName: String
    public var phones: [String]
    
    // MARK: System
    
    public init(contact: CNContact) {
        givenName = contact.givenName
        familyName = contact.familyName
        phones = []
        contact.phoneNumbers.forEach { phones.append($0.value.stringValue.onlyNumbers().changeFirstEight(to: Character("7"))) }
    }
    
}
