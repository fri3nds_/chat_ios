//
//  File.swift
//  Simple
//
//  Created by Nikita Bondar on 12.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

public class Message {
    
    public var id: String
    public var text: String
    public var userId: String
    public var chatId: String
    public var date: Int
    public var messages: [Message]
    
    public init(id: String, text: String, userId: String, chatId: String, date: Int, messages: [Message]) {
        self.id = id
        self.text = text
        self.userId = userId
        self.chatId = chatId
        self.date = date
        self.messages = messages
    }
    
    public func messageIds() -> [String] {
        var ids: [String] = []
        messages.forEach { ids.append($0.id) }
        return ids
    }
    
}
