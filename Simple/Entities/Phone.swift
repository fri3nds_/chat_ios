//
//  Phone.swift
//  Simple
//
//  Created by Nikita Bondar on 07.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

public class Phone {
    
    // MARK: Properies
    
    public var number: String
    
    public var description: String {
        return "\n======== Phone ========\n" +
            "number: \(number)\n" +
            "=======================\n"
    }
    
    // MARK: System methods
    
    public init(number: String) {
        self.number = number
    }
    
}
