//
//  Chat.swift
//  Simple
//
//  Created by Nikita Bondar on 12.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

public class Chat {
    
    // MARK: Types
    
    public enum Types: Int {
        case dialog     = 1
        case group      = 2
        case channel    = 3
    }
    
    // MARK: Properties
    
    public var id: String
    public var title: String
    public var type: Types
    public var created: Int
    public var users: [String]
    public var admins: [String]
    public var lastUpdate: Int
    public var lastUpdateFriend: Int
    public var badgeCount: Int
    
    public init(id: String, title: String, type: Int, created: Int, users: [String], admins: [String], lastUpdate: Int, lastUpdateFriend: Int, badgeCount: Int) {
        self.id = id
        self.title = title
        self.type = Types(rawValue: type)!
        self.created = created
        self.users = users
        self.admins = admins
        self.lastUpdate = lastUpdate
        self.lastUpdateFriend = lastUpdateFriend
        self.badgeCount = badgeCount
    }
    
}
