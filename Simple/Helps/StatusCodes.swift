//
//  File.swift
//  Simple
//
//  Created by Nikita Bondar on 08.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

public enum StatusCodes: Int {
    case Ok                 = 200
    case PermissionDenied   = 403
}
