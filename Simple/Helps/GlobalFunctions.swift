//
//  GlobalFunctions.swift
//  Simple
//
//  Created by Nikita Bondar on 03.09.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

public func delayTo(seconds: Double, completion: @escaping (() -> Void)) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
        completion()
    }
}
