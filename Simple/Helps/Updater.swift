//
//  Updater.swift
//  Simple
//
//  Created by Nikita Bondar on 03.09.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import Foundation

class Updater {
    
    // MARK: Properties
    
    private static var help: Updater?
    public static var shared: Updater {
        if let upd = help {
            return upd
        }
        help = Updater()
        return help!
    }
    
    private var objectsToUpdate: [Int: () -> Void] = [:]
    
    // MARK: Methods
    
    // method, which worked with tab bar controller
    public func addObject(index: Int, method: @escaping () -> Void) {
        objectsToUpdate[index] = method
    }
    
    public func update(index: Int) {
        if let method = objectsToUpdate[index] {
            method()
        }
    }
    
}
