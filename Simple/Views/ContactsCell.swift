//
//  ContactsTableViewCell.swift
//  Simple
//
//  Created by Nikita Bondar on 11.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class ContactsCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView! {
        didSet {
            avatar.rounded()
        }
    }
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var status: UILabel!
    
    // MARK: Cell methods

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
