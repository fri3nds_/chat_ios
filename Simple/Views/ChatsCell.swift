//
//  ChatsCell.swift
//  Simple
//
//  Created by Nikita Bondar on 16.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class ChatsCell: UITableViewCell {
    
    @IBOutlet weak var chatImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lastUpdate: UILabel!
    @IBOutlet weak var lastMessageText: UILabel!
    @IBOutlet weak var badge: Badge!
    
    // MARK: Cell methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: Methods
    
    public func updateBadge(value: Int) {
        badge.text = String(value)
        badge.isHidden = value == 0
        
        let maxWidth: CGFloat = 40.0
        let minWidth: CGFloat = 16.0
        
        let width = badge.text!.width(withConstrainedHeight: 16.0, font: badge.font) + 8.0
        badge.constraints.forEach { constraint in
            if constraint.identifier == "badge width" {
                constraint.constant = max(min(width, maxWidth), minWidth)
            }
        }
    }
    
}
