//
//  CircleLoader.swift
//  Simple
//
//  Created by Nikita Bondar on 30.08.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class CircleLoader: UIView {
    
    public var shape: CAShapeLayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        transform = CGAffineTransform(rotationAngle: -.pi/2)
        installShape()
        layer.addSublayer(shape)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        transform = CGAffineTransform(rotationAngle: -.pi/2)
        installShape()
        layer.addSublayer(shape)
    }
    
    public func startAnimating() {
        shape.opacity = 1.0
        
        let start = CABasicAnimation(keyPath: "strokeStart")
        start.fromValue = -0.8
        start.toValue = 1.0
        
        let end = CABasicAnimation(keyPath: "strokeEnd")
        end.fromValue = 0.0
        end.toValue = 1.0

        let rotate = CABasicAnimation(keyPath: "transform.rotation")
        rotate.fromValue = 0
        rotate.toValue = 2 * CGFloat.pi
        rotate.duration = 2.4
        rotate.repeatCount = .infinity
        
        let group = CAAnimationGroup()
        group.duration = 1.2
        group.repeatCount = .infinity
        group.animations = [start, end]
        
        shape.add(group, forKey: "loading")
        shape.add(rotate, forKey: "rotate")
    }
    
    public func stopAnimating() {
        shape.removeAnimation(forKey: "loading")
        shape.removeAnimation(forKey: "rotate")
        shape.opacity = 0.0
    }
    
    private func installShape() {
        shape = CAShapeLayer()
        shape.frame = bounds
        
        shape.fillColor = UIColor.clear.cgColor
        shape.strokeColor = UIColor.black.cgColor
        shape.lineWidth = 2.0
        shape.path = UIBezierPath(ovalIn: bounds).cgPath
        
        shape.opacity = 0.0
    }
    
}
