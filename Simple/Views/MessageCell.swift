//
//  MessageCell.swift
//  Simple
//
//  Created by Nikita Bondar on 07.09.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class MessageCell: UICollectionViewCell {
    
    // MARK: Properties
    
    public var isReaded: Bool = false {
        didSet {
            unreadFlag.isHidden = isReaded
        }
    }
    
    public var showTime: ((_ progress: CGFloat, _ layer: CALayer) -> Void)?
    public var hideTime: ((_ progress: CGFloat, _ layer: CALayer) -> Void)?
    
    // MARK: Outlets and actions
    
    @IBOutlet weak var unreadFlag: UILabel! {
        didSet {
            unreadFlag.layer.cornerRadius = 4.0
            unreadFlag.layer.backgroundColor = UIColor.lightGray.cgColor
        }
    }
    
    @IBOutlet weak var bubbleView: UIView! {
        didSet {
            bubbleView.layer.cornerRadius = 15.0
            bubbleView.layer.masksToBounds = true
            bubbleView.backgroundColor = UIColor(white: 0.5, alpha: 1)
        }
    }
    @IBOutlet weak var messageView: UITextView! {
        didSet {
            messageView.textColor = .white
        }
    }
    
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var bubbleHeight: NSLayoutConstraint!
    @IBOutlet weak var bubbleWidth: NSLayoutConstraint!

    @IBOutlet weak var messageHeight: NSLayoutConstraint!
    @IBOutlet weak var messageWidth: NSLayoutConstraint!
    
    // MARK: Methods
    
    public func update(size: CGSize) {
        let estimated = estimatedSize(allowedSize: size, koef: 0.7)
        messageWidth.constant = estimated.width + 16
        messageHeight.constant = estimated.height + 20
        bubbleWidth.constant = estimated.width + 16 + 8
        bubbleHeight.constant = estimated.height + 20
    }
    
    public func estimatedSize(allowedSize: CGSize, koef: CGFloat) -> CGSize {
        let textSize = messageView.font!.pointSize
        let estimated = NSString(string: messageView.text).boundingRect(
            with: CGSize(width: allowedSize.width * koef, height: allowedSize.height),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.systemFont(ofSize: textSize)],
            context: nil
        )
        return estimated.size
    }
    
    public func update(date: Int) {
        time.text = Date.messageTime(date: date)
    }
    
}
