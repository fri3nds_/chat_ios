//
//  Badge.swift
//  Simple
//
//  Created by Nikita Bondar on 05.09.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class Badge: UILabel {
    
    // MARK: Properties
    
    private let topInset: CGFloat = 0
    private let rightInset: CGFloat = 4
    private let bottomInset: CGFloat = 0
    private let leftInset: CGFloat = 4
    
    override public var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }

    // MARK: Label methods
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }

}
