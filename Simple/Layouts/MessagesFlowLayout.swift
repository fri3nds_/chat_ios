//
//  MessagesFlowLayout.swift
//  Simple
//
//  Created by Nikita Bondar on 15.07.2018.
//  Copyright © 2018 Nikita Bondar. All rights reserved.
//

import UIKit

class MessagesFlowLayout: UICollectionViewFlowLayout {

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)!
        var attrs: [UICollectionViewLayoutAttributes] = []
        attributes.forEach {
            let attr = $0.copy() as! UICollectionViewLayoutAttributes
            attr.transform = CGAffineTransform(rotationAngle: .pi)
            attrs.append(attr)
        }
        return attrs
    }
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attribute = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath)
        let attr = attribute?.copy() as? UICollectionViewLayoutAttributes
        attr?.transform = CGAffineTransform(rotationAngle: .pi)
        return attr
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        invalidateLayout(with: invalidationContext(forBoundsChange: newBounds))
        return super.shouldInvalidateLayout(forBoundsChange: newBounds)
    }
    
    override func invalidateLayout() {
        super.invalidateLayout()
        collectionView?.updateScrollInsets()
        
        if let collectionView = collectionView {
            let size = collectionView.bounds.size
            collectionView.visibleCells.forEach { cell in
                if let cell = cell as? MessageCell {
                    cell.update(size: CGSize(width: size.width, height: 5000.0))
                }
                if let cell = cell as? FriendMessageCell {
                    cell.update(size: CGSize(width: size.width, height: 5000.0))
                }
                cell.layoutIfNeeded()
            }
        }
    }
    
}
